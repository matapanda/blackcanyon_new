<?php
class C_landing_page extends CI_Controller
{
  function __construct()
  {
      parent::__construct();
  }
   
  function index(){
      $this->load->view('landingpagefoto_view');
  }
   
   

  function about_us(){
      $this->load->view('about_us_new_view');
  }
   

  
  function menu(){
      $this->load->view('menu_new_view');
  }
    function bukumenu(){
      $this->load->view('menu_all_view');
  }
  function menufoods(){
      $this->load->view('pdf_menu_foods');
  }
  function menudrinks(){
      $this->load->view('pdf_menu_drinks');
  }

   function blog(){
      $this->load->view('blog_view');
  }
   function blog_detail(){
      $this->load->view('blog_details_view');
  }
  function blog_detail2(){
      $this->load->view('blog_details_view2');
  }
  function blog_detail3(){
      $this->load->view('blog_details_view3');
  }
   function blog_detail4(){
      $this->load->view('blog_details_view4');
  }
   function blog_detail5(){
      $this->load->view('blog_details_view5');
  }
   function blog_detail6(){
      $this->load->view('blog_details_view6');
  }
   function blog_detail7(){
      $this->load->view('blog_details_view7');
  }
   function blog_detail8(){
      $this->load->view('blog_details_view8');
  }
   function blog_detail9(){
      $this->load->view('blog_details_view9');
  }


function kontak(){
      $this->load->view('contact_view');
  }
}