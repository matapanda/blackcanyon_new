<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="155 characters of message matching text with a call to action goes here">
    <meta name="author" content="">
    <title>OUR MENU</title>
    <link rel="shortcut icon" href="<?= base_url(); ?>assets/template_white/images/favicon.png" type="image/x-icon">
    <link rel="icon" href="<?= base_url(); ?>assets/template_white/images/favicon.png" type="image/x-icon">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/animate.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/formValidation.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/webfont.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/owl.theme.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/fonts.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/style.css">
</head>

<body class="portfolio-page">
    <div class="grid_sys hidden" style="background-image:url('grid.png'); position:fixed; left:0;right:0;top:0;bottom:0; width:100%;z-index:9999999999999999999999999999999;min-height:1024px; background-position:center center;"></div>
  
    <div class="loader">
        <div class="loader-brand">
            
            <svg viewBox="0 0 1300 300">
        <!-- Symbol-->
        <symbol id="s-text"> 
            <text text-anchor="middle" x="50%" y="50%" dy=".35em">
                <tspan   class="bold">B</tspan >LACK<tspan  class="bold">CANYON</tspan >
            </text> 
        </symbol>
        <!-- Duplicate symbols-->
        <use class="text" xlink:href="#s-text"></use>
        <use class="text" xlink:href="#s-text"></use>
        <use class="text" xlink:href="#s-text"></use>
        <use class="text" xlink:href="#s-text"></use>
        <use class="text" xlink:href="#s-text"></use>
        </svg>

        </div>
    </div>
    <header class="header">
        <div class="top-container">
            <div class="navbar-primary affixnav" data-spy="affix" data-offset-top="100">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                        <div class="logo-image">
                            <a href="#" class="brand js-target-scroll">
                                <img src="<?= base_url(); ?>assets/template_white/images/logo-white.png" alt="logo-image" class="logo-normal" />
                                <img src="<?= base_url(); ?>assets/template_white/images/logo-blacks.png" alt="logo-image" class="logo-hover" />
                            </a>
                        </div>
                    </div>
                    <div class="collapse navbar-collapse" id="navbar-collapse">
                        <ul class="nav navbar-nav navbar-right underline">
                            <li>
                                <a href="<?= base_url(); ?>" class="js-target-scroll">HOME</a>
                            </li>
                            <li>
                                <a href="<?= base_url(); ?>about_us" class="js-target-scroll">ABOUT US</a>
                            </li>
                            <li  class="active dropdown">
                                <a href="<?= base_url(); ?>menu" class="js-target-scroll">MENU</a>
                            </li>
                            <li>
                                <a href="<?= base_url(); ?>blog" class="js-target-scroll">BLOG</a>
                            </li>
                            <li>
                                <a href="<?= base_url(); ?>kontak" class="js-target-scroll">CONTACT US</a>
                            </li>     
                        </ul>

                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- banner starts -->
    <section id="banner" class="banner">
        <div class="layer">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  text-center">
                        <div class="blog-text">
                            <h1 class="banner_heading">OUR MENU</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- banner ends -->
    <!-- portfolio-details -->
    <!-- portfolio starts -->
    <section id="portfolio" class="portfolio">
      
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="portfolio-grid">
                        <div class="grid-sizer"></div>
                        <!-- 1 -->
                        <div class="illustrator grid-item grid-item--width2">
                            <div class="image-holder">
                                <img alt="" src="<?= base_url(); ?>assets/template_white/images/produk/menu/DSC03257.jpg" class="img-responsive">
                            </div>
                            <figcaption>
                                <div class="showcase-lead">
                                    <h3 class="showcase-category">Glass Noodle Pad Thai<br></h3>
                                     <span class="showcase-title">NOODLE</span>
                                    <div class="showcase-cntrls">
                                        <a href="<?= base_url(); ?>bukumenu" class="icon-round icon-round-medium"><i class="icon dripicons-enter"></i></a>
                                    </div>
                                </div>
                               
                            </figcaption>
                        </div>
                         <div class="illustrator grid-item grid-item--width2">
                            <div class="image-holder">
                                <img alt="" src="<?= base_url(); ?>assets/template_white/images/produk/menu/DSC03266.jpg" class="img-responsive">
                            </div>
                            <figcaption>
                                <div class="showcase-lead">
                                    <h3 class="showcase-category">Crispy Prawn Salad<br></h3>
                                   <span class="showcase-title">SALAD</span>
                                    <div class="showcase-cntrls">
                                        <a href="<?= base_url(); ?>bukumenu" class="icon-round icon-round-medium"><i class="icon dripicons-enter"></i></a>
                                    </div>
                                </div>
                              
                            </figcaption>
                        </div>
                         <div class="illustrator grid-item grid-item--width2">
                            <div class="image-holder">
                                <img alt="" src="<?= base_url(); ?>assets/template_white/images/produk/menu/DSC03334.jpg" class="img-responsive">
                            </div>
                            <figcaption>
                                <div class="showcase-lead">
                                    <h3 class="showcase-category">NASI GORENG SATE SPECIAL<br></h3>
                                    <span class="showcase-title">INDONESIAN FAVOURITE</span>
                                    <div class="showcase-cntrls">
                                        <a href="<?= base_url(); ?>bukumenu" class="icon-round icon-round-medium"><i class="icon dripicons-enter"></i></a>
                                    </div>
                                </div>
                               
                            </figcaption>
                        </div>
                         <div class="illustrator grid-item grid-item--width2">
                            <div class="image-holder">
                                <img alt="" src="<?= base_url(); ?>assets/template_white/images/produk/menu/DSC03385.jpg" class="img-responsive">
                            </div>
                            <figcaption>
                                <div class="showcase-lead">
                                    <h3 class="showcase-category">CHICKEN SPRIG ROLLS<br></h3>
                                    <span class="showcase-title">APPETIZERS</span>
                                    <div class="showcase-cntrls">
                                        <a href="<?= base_url(); ?>bukumenu" class="icon-round icon-round-medium"><i class="icon dripicons-enter"></i></a>
                                    </div>
                                </div>
                               
                            </figcaption>
                        </div>
                          <div class="illustrator grid-item grid-item--width2">
                            <div class="image-holder">
                                <img alt="" src="<?= base_url(); ?>assets/template_white/images/produk/menu/DSC03427.jpg" class="img-responsive">
                            </div>
                            <figcaption>
                                <div class="showcase-lead">
                                    <h3 class="showcase-category">ICED CHOCOLATE<br></h3>
                                    <span class="showcase-title">CHOC & CHILL   </span>
                                    <div class="showcase-cntrls">
                                        <a href="<?= base_url(); ?>bukumenu" class="icon-round icon-round-medium"><i class="icon dripicons-enter"></i></a>
                                    </div>
                                </div>
                               
                            </figcaption>
                        </div>
                        <!-- 2 -->
                        <div class="illustrator grid-item grid-item--height2">
                            <div class="image-holder">
                                <img alt="" src="<?= base_url(); ?>assets/template_white/images/produk/menu/wow3.jpeg" class="img-responsive">
                            </div>
                            <figcaption>
                                <div class="showcase-lead">
                                    <h3 class="showcase-category">CAPPUCCINO<br></h3>
                                    <span class="showcase-title">HOT COFFEE</span>
                                    <div class="showcase-cntrls">
                                        <a href="<?= base_url(); ?>bukumenu" class="icon-round icon-round-medium"><i class="icon dripicons-enter"></i></a>
                                    </div>
                                </div>
                                
                            </figcaption>
                        </div>
                         <div class="illustrator grid-item grid-item--height2">
                            <div class="image-holder">
                                <img alt="" src="<?= base_url(); ?>assets/template_white/images/produk/menu/DSC03370.jpg" class="img-responsive">
                            </div>
                            <figcaption>
                                <div class="showcase-lead">
                                    <h3 class="showcase-category">Thai Tea Glacier Frappe<br></h3>
                                   <span class="showcase-title">THAI TEA</span>
                                    <div class="showcase-cntrls">
                                        <a href="<?= base_url(); ?>bukumenu" class="icon-round icon-round-medium"><i class="icon dripicons-enter"></i></a>
                                    </div>
                                </div>
                               
                            </figcaption>
                        </div>
                        <!-- 3 -->
                        <div class="application grid-item">
                            <div class="image-holder">
                                <img alt="" src="<?= base_url(); ?>assets/template_white/images/produk/menu/DSC03409.jpg" class="img-responsive">
                            </div>
                            <figcaption>
                                <div class="showcase-lead">
                                     <h3 class="showcase-category">STRAWBERRY FROST<br></h3>
                                    <span class="showcase-title">HEALTHY CHOICE</span>
                                    <div class="showcase-cntrls">
                                        <a href="<?= base_url(); ?>bukumenu" class="icon-round icon-round-medium"><i class="icon dripicons-enter"></i></a>
                                    </div>
                                </div>
                               
                            </figcaption>
                        </div>
                        <div class="application grid-item">
                            <div class="image-holder">
                                <img alt="" src="<?= base_url(); ?>assets/template_white/images/produk/menu/DSC03283.jpg" class="img-responsive">
                            </div>
                            <figcaption>
                                <div class="showcase-lead">
                                    <h3 class="showcase-category">SPAGHETTI BOLOGNESE</h3>
                                    <span class="showcase-title">PASTA</span>
                                    <div class="showcase-cntrls">
                                        <a href="<?= base_url(); ?>bukumenu" class="icon-round icon-round-medium"><i class="icon dripicons-enter"></i></a>
                                    </div>
                                </div>
                              
                            </figcaption>
                        </div>
                        <div class="print grid-item grid-item--width2 grid-item--height2">
                            <div class="image-holder">
                                <img alt="" src="<?= base_url(); ?>assets/template_white/images/produk/menu/DSC03276.jpg" class="img-responsive">
                            </div>
                            <figcaption>
                                <div class="showcase-lead">
                                    <h3 class="showcase-category">RIB EYE STEAK WITH CHEESE GRAVY SAUCE<br></h3>
                                    <span class="showcase-title">ESSENTIAL DISH!</span>
                                    <div class="showcase-cntrls">
                                        <a href="<?= base_url(); ?>bukumenu" class="icon-round icon-round-medium"><i class="icon dripicons-enter"></i></a>
                                    </div>
                                </div>
                               
                            </figcaption>
                        </div>
                        <div class="print grid-item">
                            <div class="image-holder">
                                <img alt="" src="<?= base_url(); ?>assets/template_white/images/produk/menu/DSC03298.jpg" class="img-responsive">
                            </div>
                            <figcaption>
                                <div class="showcase-lead">
                                    <h3 class="showcase-category">MIXED VEGETABLES WITH OYSTER SAUCE<br></h3>
                                    <span class="showcase-title">A La Carte</span>
                                    <div class="showcase-cntrls">
                                        <a href="<?= base_url(); ?>bukumenu" class="icon-round icon-round-medium"><i class="icon dripicons-enter"></i></a>
                                    </div>
                                </div>
                               
                            </figcaption>
                        </div>
                        <div class="video grid-item">
                            <div class="image-holder">
                                <img alt="" src="<?= base_url(); ?>assets/template_white/images/produk/menu/DSC03400.jpg" class="img-responsive">
                            </div>
                            <figcaption>
                                <div class="showcase-lead">
                                    <h3 class="showcase-category">ICED MATCHA GREEN TEA LATTE<br></h3>
                                    <span class="showcase-title">MATCHA GREEN TEA</span>
                                    <div class="showcase-cntrls">
                                        <a href="<?= base_url(); ?>bukumenu" class="icon-round icon-round-medium"><i class="icon dripicons-enter"></i></a>
                                    </div>
                                </div>
                               
                            </figcaption>
                        </div>
                    </div>
                </div>
            </div>
             <div class="row">
                <div class="text-center">
                    <a href="<?= base_url(); ?>bukumenu" class="btn btn-load-more">OUR MENU BOOKS</a>
                </div>
            </div>
        </div>
    </section>
    <!-- portfolio-details ends -->
  <section id="footer" class="footer" style="background-color: white; padding-top: 0px; padding-bottom: 0px;">
        <div class="copyright-container">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                        <p class="copyrights boss-slider-info">&copy; 2021.All rights reserved.Powered by Black Canyon Coffee Indonesia <strong><a hef="#">blackcanyonindonesia.com</a></strong>.</p>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <div class="footer-social-icons">
                            <a href="https://www.facebook.com/blackcanyonindonesia-101323625562420" class="footer-icon-link facebook"><i class="fa fa-facebook " aria-hidden="true"></i></a>
                            <a href="https://twitter.com/IndonesiaBc" class="footer-icon-link twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                            <a href="https://www.instagram.com/blackcanyonindonesia/" class="footer-icon-link instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                            <a href="https://api.whatsapp.com/send?phone=6281383838497" class="footer-icon-link whatsapp"><i class="fa fa-whatsapp" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script src="<?= base_url(); ?>assets/template_white/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>assets/template_white/js/formValidation.min.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>assets/template_white/js/bootstrap.min.js"></script>
    <script src="<?= base_url(); ?>assets/template_white/js/wow.min.js"></script>
    <script src="<?= base_url(); ?>assets/template_white/js/imagesloaded.pkgd.js"></script>
    <script src="<?= base_url(); ?>assets/template_white/js/isotope.pkgd.min.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>assets/template_white/js/packery-mode.pkgd.min.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>assets/template_white/js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>assets/template_white/js/sliders.js"></script>


</body>

</html>
