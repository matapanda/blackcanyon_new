<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="155 characters of message matching text with a call to action goes here">
    <meta name="author" content="">
    <title>MENU DRINKS BOOKS</title>
    <link rel="shortcut icon" href="<?= base_url(); ?>assets/template_white/images/favicon.png" type="image/x-icon">
    <link rel="icon" href="<?= base_url(); ?>assets/template_white/images/favicon.png" type="image/x-icon">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/animate.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/formValidation.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/webfont.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/owl.theme.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/fonts.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/style.css">
</head>

<body class="portfolio-details-page">
    <div class="grid_sys hidden" style="background-image:url('grid.png'); position:fixed; left:0;right:0;top:0;bottom:0; width:100%;z-index:9999999999999999999999999999999;min-height:1024px; background-position:center center;"></div>
    
    <header class="header">
        <div class="top-container">
            <div class="navbar-primary affixnav" data-spy="affix" data-offset-top="100">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                       <div class="logo-image">
                            <a href="#" class="brand js-target-scroll">
                                <img src="<?= base_url(); ?>assets/template_white/images/logo-white.png" alt="logo-image" class="logo-normal" />
                                <img src="<?= base_url(); ?>assets/template_white/images/logo-blacks.png" alt="logo-image" class="logo-hover" />
                            </a>
                        </div>
                    </div>
                    <div class="collapse navbar-collapse" id="navbar-collapse">
                        <ul class="nav navbar-nav navbar-right underline">
                            <li >
                                <a href="<?= base_url(); ?>" class="js-target-scroll" style="color: black;">HOME</a>
                            </li>
                            <li>
                                <a href="<?= base_url(); ?>about_us" class="js-target-scroll" style="color: black;">ABOUT US</a>
                            </li>
                            <li class="active">
                                <a href="<?= base_url(); ?>menu" class="js-target-scroll" style="color: black;">MENU</a>
                            </li>
                            <li >
                                <a href="<?= base_url(); ?>blog" class="js-target-scroll" style="color: black;">BLOG</a>
                            </li>
                            <li>
                                <a href="<?= base_url(); ?>kontak" class="js-target-scroll" style="color: black;">CONTACT US</a>
                            </li>     
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </header>
     <!-- banner starts -->
    <section id="banner" class="banner">
       
    </section>
    <!-- banner ends -->
<section id="portfolio-details" class="portfolio-details">
       <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-offset-1 col-sm-10 col-md-offset-2 col-md-8 col-lg-offset-3 col-lg-6 text-center">
                            <div class="booss-demo-text fadeInDown wow" data-duration="1.5s">
                                <h6>A little piece of heaven</h6>
                                <h1>OUR MENU DRINKS</h1>
                                <hr class="hr-bg bg9">
                                
                            </div>
                        </div>
                    </div>
<iframe id="pdfviewer" src="https://drive.google.com/file/d/1EA9ckCyHY0ZkDG9QCKUbNd2-PAo7tLyp/preview?usp=sharing" frameborder="0" width="100%" height="800px"></iframe>
</div>
</section>


<section id="footer" class="footer" style="background-color: white; padding-top: 0px; padding-bottom: 0px;">
        <div class="copyright-container">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                        <p class="copyrights boss-slider-info">&copy; 2021.All rights reserved.Powered by Black Canyon Coffee Indonesia <strong><a hef="#">blackcanyonindonesia.com</a></strong>.</p>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <div class="footer-social-icons">
                            <a href="https://www.facebook.com/blackcanyonindonesia-101323625562420" class="footer-icon-link facebook"><i class="fa fa-facebook " aria-hidden="true"></i></a>
                            <a href="https://twitter.com/IndonesiaBc" class="footer-icon-link twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                            <a href="https://www.instagram.com/blackcanyonindonesia/" class="footer-icon-link instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                            <a href="https://api.whatsapp.com/send?phone=6281383838497" class="footer-icon-link whatsapp"><i class="fa fa-whatsapp" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script src="<?= base_url(); ?>assets/template_white/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>assets/template_white/js/formValidation.min.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>assets/template_white/js/bootstrap.min.js"></script>
    <script src="<?= base_url(); ?>assets/template_white/js/wow.min.js"></script>
    <script src="<?= base_url(); ?>assets/template_white/js/imagesloaded.pkgd.js"></script>
    <script src="<?= base_url(); ?>assets/template_white/js/isotope.pkgd.min.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>assets/template_white/js/packery-mode.pkgd.min.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>assets/template_white/js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>assets/template_white/js/sliders.js"></script>
</body>

</html>
