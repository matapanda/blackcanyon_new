<!--== Footer Start ==-->
<footer class="footer">
    <div class="footer-main">
      <div class="container">
        <div class="row">
          <div class="col-sm-6 col-md-4">
            <div class="widget widget-text">
              <div class="logo-footer">
                <a href="<?= base_url(); ?>assets/template/index.html"><img class="img-responsive" src="<?= base_url(); ?>assets/template/assets/images/logo-footer.png" alt=""></a>
              </div>
              <p><i class="icofont icofont-location-pin"></i> Jl. Ahmad Yani, Gayungan, Kec. Gayungan, Kota Surabaya, Jawa Timur</p>
              <p><i class="icofont icofont-phone"></i> 031-99860502</p>
              <p class="mt-20 mb-0"><i class="icofont icofont-email"></i><a href="<?= base_url(); ?>assets/template/#"> indonesiablackcanyon@gmail.com</a></p>
            </div>
          </div>
          <div class="col-sm-6 col-md-2">
            <div class="widget widget-text widget-links">
              <h5 class="widget-title">Location</h5>
              <p>
                  <a href="https://goo.gl/maps/GBBUPBBb7Sq1JtQU7">Surabaya</a><br>
                  <a href="https://goo.gl/maps/P8D2YAqCi8hWKYocA">Makassar</a><br>
                  <a href="https://goo.gl/maps/ijbTSAT83kT9oSjD9">Bali</a><br>      
                  </p>
              
            </div>
          </div>
          <div class="col-sm-6 col-md-3">
            <div class="widget widget-links xs-mt-20 xs-mb-20 sm-mt-20 sm-mb-20">
              <h5 class="widget-title">Company</h5>
              <ul>
                <li><a href="<?= base_url(); ?>menu">Our Menu</a></li>
                <li><a href="https://goo.gl/maps/GBBUPBBb7Sq1JtQU7">Location</a></li>
                <li><a href="<?= base_url(); ?>contact">Contach Us</a></li>
                <li><a href="<?= base_url(); ?>">Connect your worlds.</a></li>
              
              </ul>
            </div>
          </div>
          <div class="col-sm-6 col-md-3">
            <div class="widget widget-links xs-mt-20 xs-mb-20 sm-mt-20 sm-mb-20">
              <h5 class="widget-title">Media Sosial</h5>
            <ul class="sm-icon">
                <li><a class="facebook" href="#"><i class="icofont icofont-social-facebook"></i><span></span></a></li>
                <li><a class="twitter" href="#"><i class="icofont icofont-social-twitter"></i><span></span></a></li>
                <li><a class="behance" href="https://www.instagram.com/blackcanyonindonesia//"><i class="icofont icofont-social-instagram"></i><span></span></a></li>
              
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="copy-right text-center">© 2021 Black Canyon Indonesia. All rights reserved</div>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!--== Footer End ==-->

  <!--== Go to Top  ==-->
  <a href="<?= base_url(); ?>assets/template/javascript:" id="return-to-top"><i class="icofont icofont-arrow-up"></i></a>
  <!--== Go to Top End ==-->

</div>
<!--== Wrapper End ==-->

<!--== Javascript Plugins ==-->
<script src="<?= base_url(); ?>assets/template/https://maps.googleapis.com/maps/api/js?key=AIzaSyDJNGOwO2hJpJ9kz8e0UUPjZhEbgDJTTXE"></script>
<script src="<?= base_url(); ?>assets/template/assets/js/jquery.min.js"></script>
<script src="<?= base_url(); ?>assets/template/assets/js/plugins.js"></script>
<script src="<?= base_url(); ?>assets/template/assets/js/master.js"></script>

<!-- Revolution js Files -->
<script src="<?= base_url(); ?>assets/template/revolution/js/jquery.themepunch.tools.min.js"></script>
<script src="<?= base_url(); ?>assets/template/revolution/js/jquery.themepunch.revolution.min.js"></script>
<script src="<?= base_url(); ?>assets/template/revolution/js/revolution.extension.actions.min.js"></script>
<script src="<?= base_url(); ?>assets/template/revolution/js/revolution.extension.carousel.min.js"></script>
<script src="<?= base_url(); ?>assets/template/revolution/js/revolution.extension.kenburn.min.js"></script>
<script src="<?= base_url(); ?>assets/template/revolution/js/revolution.extension.layeranimation.min.js"></script>
<script src="<?= base_url(); ?>assets/template/revolution/js/revolution.extension.migration.min.js"></script>
<script src="<?= base_url(); ?>assets/template/revolution/js/revolution.extension.navigation.min.js"></script>
<script src="<?= base_url(); ?>assets/template/revolution/js/revolution.extension.parallax.min.js"></script>
<script src="<?= base_url(); ?>assets/template/revolution/js/revolution.extension.slideanims.min.js"></script>
<script src="<?= base_url(); ?>assets/template/revolution/js/revolution.extension.video.min.js"></script>
<!--== Javascript Plugins End ==-->

</body>
</html>
