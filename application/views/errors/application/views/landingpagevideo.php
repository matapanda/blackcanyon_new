<!DOCTYPE html>

<html lang="en" @import url(//db.onlinewebfonts.com/c/a97dc52b4b2059e5f04907cb7492af0b?family=Handel+Gothic);>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Black Canyon Cafe</title>
<link rel="shortcut icon" href="<?= base_url(); ?>assets/template/assets/images/favicon.ico">

<!-- Core Style Sheets -->
<link rel="stylesheet" href="<?= base_url(); ?>assets/template/assets/css/master.css">
<!-- Responsive Style Sheets -->
<link rel="stylesheet" href="<?= base_url(); ?>assets/template/assets/css/responsive.css">
<!-- Revolution Style Sheets -->
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/template/revolution/css/settings.css">
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/template/revolution/css/layers.css">
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/template/revolution/css/navigation.css">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link href="//db.onlinewebfonts.com/c/a97dc52b4b2059e5f04907cb7492af0b?family=Handel+Gothic" rel="stylesheet" type="text/css"/> 

<style type="text/css">
.card {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%,-50%);
    width: 80%;
    height: 60%;
    background: #000;
}
.card .image {
    width: 100%;
    height: 100%;
    overflow: hidden;
}
.card .image img {
    width: 100%;
    transition: .5s;
}
.card:hover .image img {
    opacity: .5;
    transform: translateX(30%);/*100%*/
}
.card .details {
    position: absolute;
    top: 0;
    left: 0;
    width: 20%;/*100%*/
    height: 60%;
    background: #800000;
    transition: .5s;
    transform-origin: left;
    transform: perspective(1000px) rotateY(-90deg);
}
.card:hover .details {
    transform: perspective(1000px) rotateY(0deg);
}
.card .details .center {
    padding: 20px;
    text-align: center;
    background: #fff;
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
}
.card .details .center h1 {
    margin: 0;
    padding: 0;
    color: #ff3636;
    line-height: 20px;
    font-size: 20px;
    text-transform: uppercase;
}
.card .details .center h1 span {
    font-size: 14px;
    color: #262626;
}
.card .details .center p {
    margin: 10px 0;
    padding: 0;
    color: #262626;
}
.card .details .center ul {
    margin: 10px auto 0;
    padding: 0;
    display: table;
}
.card .details .center ul li {
    list-style: none;
    margin: 0 5px;
    float: left;
}
.card .details .center ul li a {
    display: block;
    background: #262626;
    color: #fff;
    width: 30px;
    height: 30px;
    line-height: 30px;
    text-align: center;
    transform: .5s;
}
.card .details .center ul li a:hover {
    background: #ff3636;
}


</style>

</head>
<body>

<!--== Loader Start ==-->
<div id="loader-overlay">
  <div class="loader">
    <img src="<?= base_url(); ?>assets/template/assets/images/loader.svg" width="80" alt="">
  </div>
</div>
<!--== Loader End ==-->

<!--== Wrapper Start ==-->
<div class="wrapper">

  <!--== Header Start ==-->
  <nav class="navbar navbar-default navbar-fixed navbar-transparent white bootsnav on no-full no-border">
    <!--== Start Top Search ==-->
    <div class="fullscreen-search-overlay" id="search-overlay"> <a href="<?= base_url(); ?>assets/template/#" class="fullscreen-close" id="fullscreen-close-button"><i class="icofont icofont-close"></i></a>
      <div id="fullscreen-search-wrapper">
        <form method="get" id="fullscreen-searchform">
          <input type="text" value="" placeholder="Type and hit Enter..." id="fullscreen-search-input" class="search-bar-top">
          <i class="fullscreen-search-icon icofont icofont-search">
          <input value="" type="submit">
          </i>
        </form>
      </div>
    </div>
    <!--== End Top Search ==-->
    <div class="container">
      <!--== Start Atribute Navigation ==-->
      <div class="attr-nav no-border hidden-xs">
        <ul class="social-media-dark social-top">
          <li><a href="<?= base_url(); ?>assets/template/#" class="icofont icofont-social-facebook"></a></li>
          <li><a href="<?= base_url(); ?>assets/template/#" class="icofont icofont-social-twitter"></a></li>
          <li><a href="<?= base_url(); ?>assets/template/#" class="icofont icofont-social-linkedin"></a></li>
        </ul>
      </div>
      <!--== End Atribute Navigation ==-->

      <!--== Start Header Navigation ==-->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu"> <i class="tr-icon ion-android-menu"></i> </button>
        <div class="logo"> <a href="<?= base_url(); ?>assets/template/index.html"> <img class="logo logo-display" src="<?= base_url(); ?>assets/template/assets/images/logo-white.png" alt=""> <img class="logo logo-scrolled" src="<?= base_url(); ?>assets/template/assets/images/logo-black.png" alt=""> </a> </div>
      </div>
      <!--== End Header Navigation ==-->

      <!--== MENU HEADER==-->
      <div class="collapse navbar-collapse" id="navbar-menu">
        <ul class="nav navbar-nav navbar-center" data-in="fadeIn" data-out="fadeOut">  
          <li><a class="page-scroll" href="#">Home</a></li>
          <li><a class="page-scroll" href="#">About Us</a></li>
          <li><a class="page-scroll" href="#">Menu</a></li>
          <li><a class="page-scroll" href="<?= base_url(); ?>blog">Blog</a></li>
          <li><a class="page-scroll" href="<?= base_url(); ?>assets/template/#contact">Contact</a></li>
        </ul>
      </div>
      <!--== /.navbar-collapse ==-->
    </div>

  </nav>
  <!--== Header End ==-->


  <!--== Hero Slider Start ==-->
  <section class="remove-padding relative view-height-100vh white-bg" id="home">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12 display-table view-height-100vh">
          <div class="v-align-middle text-center hero-text">
            <div class="white-color" >
              <h1 class="font-800" style=" font-family: Handel Gothic;">Black Canyon Indonesia</h1>
              <h2 class="font-400">“A taste from paradise, available on earth”</h2>
           
            </div>
          </div>
        </div>
      </div>
    </div>
    <video autoplay="" muted="" loop="" controls="" class="html5-video">
      <source src="<?= base_url(); ?>assets/template/assets/videos/bc.mp4" type="video/mp4">
      <source src="<?= base_url(); ?>assets/template/assets/videos/bc.webm" type="video/webm">
    </video>
  </section>
  <!--== Hero Slider End ==-->



  <!--=== About Us Start ======-->
  <section style="background:url(assets/images/background/parallax-bg.png) center center no-repeat #800000;" class="height-800px" >
       <div class="col-md-6 col-sm-6 bg-flex bg-flex-right">
      <div class="bg-flex-holder bg-flex-cover" style="background-image: url(<?= base_url(); ?>assets/template/assets/images/bannerbc2.png);"></div>
    </div>
    <div class="container-fluid">
      <div class="col-md-5 col-sm-6 pl-70 pr-70">
        <div class="section-title">
        
          <h1 class="white-color">About Us</h1>
          <hr class="left-line white-bg">
        </div>
        <p class="white-color" style="text-align: justify;">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
        <p class="mt-30"><a class="btn btn-lg btn-light-outline btn-rounded">Read more</a></p>
      </div>
    </div>
  </section>
  <!--=== About Us  End ======-->

<!--=== About Us Menu ======-->
<!-- 1 -->
<section class="lg-section" id="about" >
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12">
          <img src="<?= base_url(); ?>assets/template/assets/images/gallery/roti.png" class="img-responsive"  style="border-radius: 100%; width: 80%" />
        
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12 xs-mb-50">
          <div class="section-title text-left">
            <h1 style="font: italic 4em Fira Sans serif;">Paradise on <br>Your Plate</h1>
            <p class="mt-50 font-16px line-height-20" style="text-align: left;">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
             <p class="mt-30"><a class="btn btn-lg btn-dark-outline btn-rounded">Read more</a></p>
          </div>
        </div>

        
      </div>
    </div>
  </section>

<!-- 2 -->
  <section class="lg-section" id="about" >
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12 xs-mb-50">
          <div class="section-title text-left">
            <h1 style="font: italic 4em Fira Sans serif;">Paradise on <br>Your Plate</h1>
            <p class="mt-50 font-16px line-height-20" style="text-align: left;">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
             <p class="mt-30"><a class="btn btn-lg btn-dark-outline btn-rounded">Read more</a></p>
          </div>
        </div>

        <div class="col-md-6 col-sm-6 col-xs-12">
          <img src="<?= base_url(); ?>assets/template/assets/images/gallery/roti.png" class="img-responsive" style="border-radius: 100%; width: 80%" />
        
        </div>
      </div>
  </section>
  <!--=== About Us End ======-->


  <!--== artikel/blog  ==-->
  <section style="background:url(assets/images/background/parallax-bg.png) center center no-repeat #800000;" >
    <div class="col-md-12 col-sm-12 col-xs-12 display-table view-height-100vh">
         
        </div>
       <div class="card">
    <div class="image">
      <img src="<?= base_url(); ?>assets/template/assets/images/gallery/bcsby.png" style="width: 100%"/>
    </div>
    <div class="details display-table view-height-100vh">
      <div class="center">
        <h1>Judul Artikel<br><span>team leader</span></h1>
        <p>Lorem ipsum is simple dummy text on the printing and typesetting industry.</p>
          <p class="mt-30"><a class="btn btn-lg btn-dark-outline btn-rounded">Read more</a></p>
      </div>
    </div>
  </section>
  

  <!--== Google Map Start ==-->
  <div class="transition-none pt-0 pb-0 light-bg cover-bg">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
         
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3957.8036579294558!2d112.73728011537827!3d-7.263172573378868!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd7fca5b4a11e21%3A0x235fc9989f91585a!2sLight%20-%20Cafe%20and%20Eatery%20(%20d%2Fh%20Black%20Canyon%20Coffee%20)!5e0!3m2!1sid!2sid!4v1624690248817!5m2!1sid!2sid" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
        
        </div>
      </div>
    </div>
  </div>
  <!--== Google Map End ==-->

  <!--== Footer Start ==-->
  <footer class="footer">
    <div class="footer-main">
      <div class="container">
        <div class="row">
          <div class="col-sm-6 col-md-3">
            <div class="widget widget-text">
              <div class="logo-footer">
                <a href="<?= base_url(); ?>assets/template/index.html"><img class="img-responsive" src="<?= base_url(); ?>assets/template/assets/images/logo-footer.png" alt=""></a>
              </div>
              <p>Tunjungan Plaza 3, Lantai 4, Jl. Basuki Rahmat No. 8 - 12, Kedungdoro, Tegalsari, Surabaya, Jawa Timur</p>
              <p class="mt-20 mb-0"><a href="<?= base_url(); ?>assets/template/#">+62315 317 851</a></p>
              <p class="mt-0"><a href="<?= base_url(); ?>assets/template/https://www.google.com/maps/search/Potsdamer+Platz+9797/@52.5096488,13.3737554,17z/data=!3m1!4b1" target="_blank">Black Canyon</a></p>
            </div>
          </div>
          <div class="col-sm-6 col-md-3">
            <div class="widget widget-text widget-links">
              <h5 class="widget-title">Location</h5>
              <p>
                  <a href="#">Surabaya</a><br>
                  <a href="#">Makassar</a><br>
                  <a href="#">Bali</a><br>
                
                  </p>
              
            </div>
          </div>
          <div class="col-sm-6 col-md-3">
            <div class="widget widget-links xs-mt-20 xs-mb-20 sm-mt-20 sm-mb-20">
              <h5 class="widget-title">Company</h5>
              <ul>
                <li><a href="#">Our Menu</a></li>
                <li><a href="#">Location</a></li>
                <li><a href="#">Contach Us</a></li>
                <li><a href="#">Connect your worlds.</a></li>
              
              </ul>
            </div>
          </div>
          <div class="col-sm-6 col-md-3">
            <div class="widget widget-links xs-mt-20 xs-mb-20 sm-mt-20 sm-mb-20">
              <h5 class="widget-title">Media Sosial</h5>
            <ul class="social-media-dark social-top">
          <li><a href="<?= base_url(); ?>assets/template/#" class="icofont icofont-social-instagram"></a></li>
          <li><a href="<?= base_url(); ?>assets/template/#" class="icofont icofont-social-twitter"></a></li>
          <li><a href="<?= base_url(); ?>assets/template/#" class="icofont icofont-social-facebook"></a></li>
        </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="copy-right text-center">© 2021 Black Canyon Indonesia. All rights reserved</div>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!--== Footer End ==-->

  <!--== Go to Top  ==-->
  <a href="javascript:" id="return-to-top"><i class="icofont icofont-arrow-up"></i></a>
  <!--== Go to Top End ==-->

</div>
<!--== Wrapper End ==-->

<!--== Javascript Plugins ==-->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDJNGOwO2hJpJ9kz8e0UUPjZhEbgDJTTXE"></script>
<script src="<?= base_url(); ?>assets/template/assets/js/jquery.min.js"></script>
<script src="<?= base_url(); ?>assets/template/assets/js/plugins.js"></script>
<script src="<?= base_url(); ?>assets/template/assets/js/master.js"></script>

<!-- Revolution js Files -->
<script src="<?= base_url(); ?>assets/template/revolution/js/jquery.themepunch.tools.min.js"></script>
<script src="<?= base_url(); ?>assets/template/revolution/js/jquery.themepunch.revolution.min.js"></script>
<script src="<?= base_url(); ?>assets/template/revolution/js/revolution.extension.actions.min.js"></script>
<script src="<?= base_url(); ?>assets/template/revolution/js/revolution.extension.carousel.min.js"></script>
<script src="<?= base_url(); ?>assets/template/revolution/js/revolution.extension.kenburn.min.js"></script>
<script src="<?= base_url(); ?>assets/template/revolution/js/revolution.extension.layeranimation.min.js"></script>
<script src="<?= base_url(); ?>assets/template/revolution/js/revolution.extension.migration.min.js"></script>
<script src="<?= base_url(); ?>assets/template/revolution/js/revolution.extension.navigation.min.js"></script>
<script src="<?= base_url(); ?>assets/template/revolution/js/revolution.extension.parallax.min.js"></script>
<script src="<?= base_url(); ?>assets/template/revolution/js/revolution.extension.slideanims.min.js"></script>
<script src="<?= base_url(); ?>assets/template/revolution/js/revolution.extension.video.min.js"></script>

<script type="text/javascript">
  var swiper = new Swiper('.blog', {
    autoplay: {
    delay: 5000,
  }, 
     spaceBetween: 30,
      effect: 'fade',
      loop: true,
      mousewheel: {
        invert: false,
      },
       //autoHeight: true,
      pagination: {
        el: '.blog-slider__pagination',
        clickable: true,
      }
    });


</script>
<!--== Javascript Plugins End ==-->

</body>
</html>
