<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="155 characters of message matching text with a call to action goes here">
    <meta name="author" content="">
    <title>CONTACT US</title>
    <link rel="shortcut icon" href="<?= base_url(); ?>assets/template_white/images/favicon.png" type="image/x-icon">
    <link rel="icon" href="<?= base_url(); ?>assets/template_white/images/favicon.png" type="image/x-icon">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/animate.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/formValidation.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/webfont.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/owl.theme.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/fonts.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/style.css">
</head>

<body class="services_page">
    
    <div class="grid_sys hidden" style="background-image:url('grid.png'); position:fixed; left:0;right:0;top:0;bottom:0; width:100%;z-index:9999999999999999999999999999999;min-height:1024px; background-position:center center;"></div>
    <div class="loader">
        <div class="loader-brand">
            
            <svg viewBox="0 0 1300 300">
        <!-- Symbol-->
        <symbol id="s-text"> 
            <text text-anchor="middle" x="50%" y="50%" dy=".35em">
                <tspan   class="bold">B</tspan >LACK<tspan  class="bold">CANYON</tspan >
            </text> 
        </symbol>
        <!-- Duplicate symbols-->
        <use class="text" xlink:href="#s-text"></use>
        <use class="text" xlink:href="#s-text"></use>
        <use class="text" xlink:href="#s-text"></use>
        <use class="text" xlink:href="#s-text"></use>
        <use class="text" xlink:href="#s-text"></use>
        </svg>

        </div>
    </div>
    <header class="header">
        <div class="top-container">
            <div class="navbar-primary affixnav" data-spy="affix" data-offset-top="100">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                        <div class="logo-image">
                            <a href="" class="brand js-target-scroll">
                                <img src="<?= base_url(); ?>assets/template_white/images/logo-white.png" alt="logo-image" class="logo-normal" />
                                <img src="<?= base_url(); ?>assets/template_white/images/logo-blacks.png" alt="logo-image" class="logo-hover" />
                            </a>
                        </div>
                    </div>
                     <div class="collapse navbar-collapse" id="navbar-collapse">
                        <ul class="nav navbar-nav navbar-right underline">
                            <li>
                                <a href="<?= base_url(); ?>" class="js-target-scroll">HOME</a>
                            </li>
                            <li>
                                <a href="<?= base_url(); ?>about_us" class="js-target-scroll">ABOUT US</a>
                            </li>
                            <li >
                                <a href="<?= base_url(); ?>menu" class="js-target-scroll">MENU</a>
                            </li>
                            <li >
                                <a href="<?= base_url(); ?>blog" class="js-target-scroll">BLOG</a>
                            </li>
                            <li class="active">
                                <a href="<?= base_url(); ?>kontak" class="js-target-scroll">CONTACT US</a>
                            </li>     
                        </ul>

                    </div>


                </div>

                </div>
            </div>
        </div>
    </header>
    <div class="content">
        <!-- banner starts -->
        <section id="banner" class="banner">
            <div class="layer">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  text-center">
                            <div class="blog-text">
                                <h1 class="banner_heading">SERVICES</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- banner ends -->
        <main class="main">
          
            <!-- mutipurpose starts -->
            <section id="mutipurpose" class="mutipurpose multipurpose-orange-bg">
                <div class="layer">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-5 col-lg-6">
                               <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3957.298928591898!2d112.73027941477524!3d-7.320280094716983!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd7fb6d64a418ed%3A0xf8e8c56ba435e288!2sBlack%20Canyon%20Coffee%20%26%20Eatery%20Graha%20Pena!5e0!3m2!1sid!2sid!4v1625839651705!5m2!1sid!2sid" width="90%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-7 col-lg-6">
                                <div class="mutipurpose-text fadeInRight wow" data-wow-duration="1s">
                                      <h1 style="font: bold 4em ; color:black;">Location</h1><br>
                          
                                      <a href="<?= base_url(); ?>about_us"><img class="img-responsive" src="<?= base_url(); ?>assets/template_white/images/logo-blacks.png" alt=""></a><BR>
                                    
                                      <p style=" color:black; font-size: 20px;" ><i class="fa fa-map-marker"></i> Black Canyon Coffee & Eatery Graha Pena, <br> Jl. Ahmad Yani, Gayungan, <br>Kec. Gayungan, Kota Surabaya, Jawa Timur</p>
                                      <p style=" color:black; font-size: 20px;"><i class="fa fa-whatsapp "></i><a href="https://api.whatsapp.com/send?phone=6281383838497"> +62 81383838497</a></p>
                                      <p class="mt-20 mb-0" style=" color:black; font-size: 20px;"><i class="fa fa-envelope "></i><a href="<?= base_url(); ?>assets/template/#"> indonesiablackcanyon@gmail.com</a></p>
                                       <p class="mt-20 mb-0" style=" color:black; font-size: 20px;"><i class="fa fa-clock-o "></i> OPEN 10.00 WIB</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
         
            <!-- cta starts -->
            <section id="cta" class="cta cta-gray-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-9">
                            <div class="buynow-text">
                                <h1 class="slider-heading buynow-head fadeInLeft wow">Our delish food and beverages are produced with the finest ingredients, by the best chefs and baristas, and served with pleasure. Just sit, be relax, and spoil your tongue with our special menu.</h1>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 text-right">
                            <div class="buynow-button">
                                <a href="<?= base_url(); ?>menu" class="btn btn-gray-buynow">OUR MENU</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
    </div>
 <section id="footer" class="footer" style="background-color: white; padding-top: 0px; padding-bottom: 0px;">
        <div class="copyright-container">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                        <p class="copyrights boss-slider-info">&copy; 2021.All rights reserved.Powered by Black Canyon Coffee Indonesia <strong><a hef="#">blackcanyonindonesia.com</a></strong>.</p>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <div class="footer-social-icons">
                            <a href="https://www.facebook.com/blackcanyonindonesia-101323625562420" class="footer-icon-link facebook"><i class="fa fa-facebook " aria-hidden="true"></i></a>
                            <a href="https://twitter.com/IndonesiaBc" class="footer-icon-link twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                            <a href="https://www.instagram.com/blackcanyonindonesia/" class="footer-icon-link instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                            <a href="https://api.whatsapp.com/send?phone=6281383838497" class="footer-icon-link whatsapp"><i class="fa fa-whatsapp" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script src="<?= base_url(); ?>assets/template_white/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>assets/template_white/js/formValidation.min.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>assets/template_white/js/bootstrap.min.js"></script>
    <script src="<?= base_url(); ?>assets/template_white/js/wow.min.js"></script>
    <script src="<?= base_url(); ?>assets/template_white/js/imagesloaded.pkgd.js"></script>
    <script src="<?= base_url(); ?>assets/template_white/js/isotope.pkgd.min.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>assets/template_white/js/packery-mode.pkgd.min.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>assets/template_white/js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>assets/template_white/js/sliders.js"></script>


</body>

</html>
