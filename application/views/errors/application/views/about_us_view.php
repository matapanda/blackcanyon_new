<?php
  include 'template/_header.php';
?>

 <!--== Page Title Start ==-->
 <div class="transition-none">
     <section class="title-hero-bg parallax-effect" style="background-image: url(<?= base_url(); ?>assets/template/assets/images/bcsby2.png);">
       <div class="container">
         <div class="row">
           <div class="col-md-12">
             <div class="page-title text-center white-color">
               <h1 class="font-700">About Us</h1>
               <div class="breadcrumb mt-20">
                   <!-- Breadcrumb Start -->
                       <ul>
                         <li><a href="<?= base_url(); ?>">Home</a></li>
                         <li>About Us</li>
                       </ul>
                   <!-- Breadcrumb End -->
               </div>
             </div>
           </div>
         </div>

       </div>
     </section>
 </div>
 <!--== Page Title End ==-->
 
	<!-- ABOUT US -->
<!-- 	<section style="background:url() right no-repeat #800000;" class="height-650px" style="padding-bottom:0px" >
		<div class="container-fluid">
				<div class="row">
				<div class="col-md-6 col-sm-6 pl-90 pr-70">
						<div class="section-title text-left">
							<h1 style="font: italic 4em Fira Sans serif; color:white;">About Us</h1>
							<p class="mt-50 font-16px line-height-20" style="text-align: justify; color: white;">
							Since its establishment in 1993, Black Canyon has continued to provide an unique and memorable dining experience with delicious Asian cuisine. Our delightful food and beverages are produced with the finest ingredients, by the best chefs and baristas, and served with pleasure. We are delighted to provide you a wonderful place filled with the aroma of coffee, delicious food and beautiful surroundings, in which you may spend quality time with family and friends. </p>
							<p class="mt-30"><a href="" class="btn btn-lg btn-light-outline btn-rounded">Read more</a></p>
						</div>
					</div>

					<div class="col-md-6 col-sm-6 col-xs-12">
						<img src="" class="img-responsive" style="border-radius: 0%; width: 100%" />
					
					</div>
				</div>
	</section> -->
	<!-- END ABOUT US -->


  <!-- ABOUT US -->
    <section style="background:url() right no-repeat #fff;" class="" style="padding-bottom:0px" >
    <div class="container">
        <div class="row">
        <div class="col-md-6 col-sm-6 pl-90 pr-70">
            <div class="section-title text-left">
              <h1 style="font: italic 4em Fira Sans serif; color:black;">About Us</h1>
              <p class="" style="text-align: justify; color: black;">
              <h3>What kind of place are you looking for?</h3><br>
              <h4>A getaway, a break, or a place to work?</h4><br>
              It's all here at Black Canyon Indonesia.<br>
              Since our establishment in 1993, we make sure to create an exciting and unforgettable moment through wonderful Asian food experience and specialty coffee from Indonesia.
              This is the right place to find bright ideas, innovative steps, and harmonious collaboration with colleagues, all in a peaceful ambience with a touch of Thailand’s unique culture. If you’re looking for some quality time, we have a well-designed environment with a beautiful serene atmosphere.
              <h3>What a perfect destination, isn’t it?</h3></p>
              <p class="mt-30"><a href="<?= base_url(); ?>c_landing_page/menu" class="btn btn-lg btn-dark-outline btn-rounded">Our Menu</a></p>
            </div>
          </div>

          <div class="col-md-6 col-sm-6 col-xs-12">
            <img src="<?= base_url(); ?>assets/template/assets/images/surabaya_bc.png" class="img-responsive" style="border-radius: 0%; width: 100%" />
          
          </div>
        </div>
  </section>
  <!-- END ABOUT US -->

<?php
  include 'template/_footer.php';
?>