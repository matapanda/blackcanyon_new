<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="155 characters of message matching text with a call to action goes here">
    <meta name="author" content="">
    <title>ABOUT US</title>
    <link rel="shortcut icon" href="<?= base_url(); ?>assets/template_white/images/favicon.png" type="image/x-icon">
    <link rel="icon" href="<?= base_url(); ?>assets/template_white/images/favicon.png" type="image/x-icon">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/animate.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/formValidation.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/webfont.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/owl.theme.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/fonts.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/style.css">
</head>

<body class="about_page">
    <div class="grid_sys hidden" style="background-image:url('grid.png'); position:fixed; left:0;right:0;top:0;bottom:0; width:100%;z-index:9999999999999999999999999999999;min-height:1024px; background-position:center center;"></div>
    <div class="loader">
        <div class="loader-brand">
            
            <svg viewBox="0 0 1300 300">
        <!-- Symbol-->
        <symbol id="s-text"> 
            <text text-anchor="middle" x="50%" y="50%" dy=".35em">
                <tspan   class="bold">B</tspan >LACK<tspan  class="bold">CANYON</tspan >
            </text> 
        </symbol>
        <!-- Duplicate symbols-->
        <use class="text" xlink:href="#s-text"></use>
        <use class="text" xlink:href="#s-text"></use>
        <use class="text" xlink:href="#s-text"></use>
        <use class="text" xlink:href="#s-text"></use>
        <use class="text" xlink:href="#s-text"></use>
        </svg>

        </div>
    </div>
    <header class="header">
        <div class="top-container">
            <div class="navbar-primary affixnav" data-spy="affix" data-offset-top="100">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                       <div class="logo-image">
                            <a href="<?= base_url(); ?>" class="brand js-target-scroll">
                                <img src="<?= base_url(); ?>assets/template_white/images/logo-white.png" alt="logo-image" class="logo-normal" />
                                <img src="<?= base_url(); ?>assets/template_white/images/logo-blacks.png" alt="logo-image" class="logo-hover" />
                            </a>
                        </div>
                    </div>
                     <div class="collapse navbar-collapse" id="navbar-collapse">
                        <ul class="nav navbar-nav navbar-right underline">
                            <li >
                                <a href="<?= base_url(); ?>" class="js-target-scroll">HOME</a>
                            </li>
                            <li class="active dropdown">
                                <a href="<?= base_url(); ?>about_us" class="js-target-scroll">ABOUT US</a>
                            </li>
                            <li >
                                <a href="<?= base_url(); ?>menu" class="js-target-scroll">MENU</a>
                            </li>
                            <li >
                                <a href="<?= base_url(); ?>blog" class="js-target-scroll">BLOG</a>
                            </li>
                            <li>
                                <a href="<?= base_url(); ?>kontak" class="js-target-scroll">CONTACT US</a>
                            </li>     
                        </ul>

                    </div>

                </div>
            </div>
        </div>
    </header>
    <div class="content">
        <!-- banner starts -->
        <section id="banner" class="banner">
            <div class="layer">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  text-center">
                            <div class="blog-text">
                                <h1 class="banner_heading">ABOUT US</h1>
                       
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- banner ends -->
        <main class="main">
            
            <!-- mutipurpose starts -->
            <section id="mutipurpose" class="mutipurpose" style="background-color: white; padding-bottom: 0px">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-5 col-lg-6" style="padding-top: 0px;">
                            <!--  <h1 style="color: black; font-family:Roboto, sans-serif; font-weight: 600">BLACK CANYON INDONESIA</h1><br> -->
                            <img class="img-responsive" src="<?= base_url(); ?>assets/template_white/images/produk/about_us2.png" alt="history-image" />
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-7 col-lg-6">
                            <div class="mutipurpose-text fadeInRight wow" data-wow-duration="1s">
                         
                                  <div class="services-text  text-left">
                                <hr class="hr-bg bg2 stock-bg2 hidden-xs">
                                <h1 style="color: black; font-family:Roboto, sans-serif; font-weight: 600">ABOUT US</h1><br>
                                <p style="color: grey; font-family:Roboto, sans-serif;"><b>What kind of place are you looking for?</b></p>
                                <p style="color: grey; font-family:Roboto, sans-serif;"><b>A getaway, a break, or a place to work?</b></p>
                                <p style="color: grey;  font-family:Roboto, sans-serif;">It's all here at Black Canyon Indonesia.
                                <p style="text-align: justify;"> <b>Since our establishment in 1993</b>, we make sure to create an exciting and unforgettable moment through wonderful Asian food experience and specialty coffee from Indonesia.
                                  This is the right place to find <i> bright ideas, innovative steps, and harmonious collaboration with colleagues, all in a peaceful ambience with a touch of Thailand’s unique culture</i>. If you’re looking for some quality time, we have a well-designed environment with a beautiful serene atmosphere.
                                  <p style="font-family:Roboto, sans-serif;"><b>What a perfect destination, isn’t it?</b></p>
                                    <a href="<?= base_url(); ?>menu" class="btn btn-primary">OUR MENU</a>
                            </div>

                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- clients starts -->
            <section id="clients" class="clients">
                <div class="container">
                    <div class="row">
                      
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="owl-logo-carousel owl-theme">
                                <div class="item">
                                    <img class="img-responsive" src="<?= base_url(); ?>assets/template_white/images/produk/DSC03323.jpg" alt="logo-1">
                                </div>
                                  <div class="item">
                                    <img class="img-responsive" src="<?= base_url(); ?>assets/template_white/images/produk/DSC03380.jpg" alt="logo-2">
                                </div>
                                <div class="item">
                                    <img class="img-responsive" src="<?= base_url(); ?>assets/template_white/images/produk/DSC03329.jpg" alt="logo-3">
                                </div>
                                <div class="item">
                                    <img class="img-responsive" src="<?= base_url(); ?>assets/template_white/images/produk/DSC03400.jpg" alt="logo-4">
                                </div>
                                <div class="item">
                                    <img class="img-responsive" src="<?= base_url(); ?>assets/template_white/images/produk/DSC03341.jpg" alt="logo-5">
                                </div>
                              
                                
                                <div class="item">
                                    <img class="img-responsive" src="<?= base_url(); ?>assets/template_white/images/produk/wow3.jpeg" alt="logo-6">
                                </div>
                                <div class="item">
                                    <img class="img-responsive" src="<?= base_url(); ?>assets/template_white/images/produk/DSC03427.jpg" alt="logo-7">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
    </div>
<section id="footer" class="footer" style="background-color: white; padding-top: 0px; padding-bottom: 0px;">
        <div class="copyright-container">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                        <p class="copyrights boss-slider-info">&copy; 2021.All rights reserved.Powered by Black Canyon Coffee Indonesia <strong><a hef="#">blackcanyonindonesia.com</a></strong>.</p>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <div class="footer-social-icons">
                            <a href="https://www.facebook.com/blackcanyonindonesia-101323625562420" class="footer-icon-link facebook"><i class="fa fa-facebook " aria-hidden="true"></i></a>
                            <a href="https://twitter.com/IndonesiaBc" class="footer-icon-link twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                            <a href="https://www.instagram.com/blackcanyonindonesia/" class="footer-icon-link instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                            <a href="https://api.whatsapp.com/send?phone=6281383838497" class="footer-icon-link whatsapp"><i class="fa fa-whatsapp" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script src="<?= base_url(); ?>assets/template_white/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>assets/template_white/js/formValidation.min.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>assets/template_white/js/bootstrap.min.js"></script>
    <script src="<?= base_url(); ?>assets/template_white/js/wow.min.js"></script>
    <script src="<?= base_url(); ?>assets/template_white/js/imagesloaded.pkgd.js"></script>
    <script src="<?= base_url(); ?>assets/template_white/js/isotope.pkgd.min.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>assets/template_white/js/packery-mode.pkgd.min.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>assets/template_white/js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>assets/template_white/js/sliders.js"></script>


</body>

</html>
