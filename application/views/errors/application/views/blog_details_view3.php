<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="155 characters of message matching text with a call to action goes here">
    <meta name="author" content="">
    <title>BLOG-DETAILS</title>
    <link rel="shortcut icon" href="<?= base_url(); ?>assets/template_white/images/favicon.png" type="image/x-icon">
    <link rel="icon" href="<?= base_url(); ?>assets/template_white/images/favicon.png" type="image/x-icon">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/animate.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/formValidation.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/webfont.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/owl.theme.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/fonts.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/style.css">
</head>

<body class="blog-details-page">
    <div class="grid_sys hidden" style="background-image:url('grid.png'); position:fixed; left:0;right:0;top:0;bottom:0; width:100%;z-index:9999999999999999999999999999999;min-height:1024px; background-position:center center;"></div>
    <div class="loader">
        <div class="loader-brand">
            
            <svg viewBox="0 0 1300 300">
        <!-- Symbol-->
        <symbol id="s-text"> 
            <text text-anchor="middle" x="50%" y="50%" dy=".35em">
                <tspan   class="bold">B</tspan >LACK<tspan  class="bold">CANYON</tspan >
            </text> 
        </symbol>
        <!-- Duplicate symbols-->
        <use class="text" xlink:href="#s-text"></use>
        <use class="text" xlink:href="#s-text"></use>
        <use class="text" xlink:href="#s-text"></use>
        <use class="text" xlink:href="#s-text"></use>
        <use class="text" xlink:href="#s-text"></use>
        </svg>

        </div>
    </div>
    <header class="header">
        <div class="top-container">
            <div class="navbar-primary affixnav" data-spy="affix" data-offset-top="100">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                        <div class="logo-image">
                            <a href="#" class="brand js-target-scroll">
                                <img src="<?= base_url(); ?>assets/template_white/images/logo-white.png" alt="logo-image" class="logo-normal" />
                                <img src="<?= base_url(); ?>assets/template_white/images/logo-blacks.png" alt="logo-image" class="logo-hover" />
                            </a>
                        </div>
                    </div>
                    <div class="collapse navbar-collapse" id="navbar-collapse">
                        <ul class="nav navbar-nav navbar-right underline">
                            <li>
                                <a href="<?= base_url(); ?>" class="js-target-scroll">HOME</a>
                            </li>
                            <li>
                                <a href="<?= base_url(); ?>about_us" class="js-target-scroll">ABOUT US</a>
                            </li>
                            <li >
                                <a href="<?= base_url(); ?>menu" class="js-target-scroll">MENU</a>
                            </li>
                            <li class="active dropdown">
                                <a href="<?= base_url(); ?>blog" class="js-target-scroll">BLOG</a>
                            </li>
                            <li>
                                <a href="<?= base_url(); ?>kontak" class="js-target-scroll">CONTACT US</a>
                            </li>     
                        </ul>

                    </div>

                </div>
            </div>
        </div>
    </header>
    <!-- banner starts -->
    <section id="banner" class="banner">
        <div class="layer">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  text-center">
                        <div class="blog-text">
                            <h1 class="banner_heading">BLOG DETAIL</h1>
                         
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- banner ends -->
    <!-- blog-details -->
    <section id="blog_details" class="blog_post-container blog_details">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 blog-gradient-left">
                    <img class="img-responsive" src="<?= base_url(); ?>assets/template_white/images/produk/artikel3.png" alt="blog">
                    <h1 class="blog_details_head">Kenali Berbagai Keunikan <br> Cita Rasa Asia </h1>
                   
                    
                    <p style=" text-align: justify;">&ensp; &ensp; Setiap negara memiliki keunikannya sendiri. Tidak hanya bahasa, budaya, dan mode, namun juga selera masakan yang berbeda. Cara memasak, bahan makanan, dan kebiasaan tersebut menciptakan berbagai keunikan cita rasa pada setiap negara. Tidak terkecuali negara-negara di benua Asia. Tidak semua makanan Asia memiliki cita rasa yang sama. Walaupun makanan Asia terkenal dengan kulinernya yang khas menggunakan bumbu rempah yang bervariatif, namun masing-masing negara memiliki keunikannya sendiri. Nah, yuk simak berbagai keunikan dari cita rasa Asia!</p>
                    <p style="text-align: justify;"><b>Cina</b><br>
                    Wilayah yang luas, perbedaan budaya dan iklim negara, membuat masakan Cina paling beragam di antara yang lain. Namun, faktanya pada keberagaman tersebut, orang Cina lebih memilih untuk tetap berpegang pada resep yang sudah teruji dan turun menurun. Masakan Cina memiliki ciri khas menggunakan berbagai sayuran seperti tauge, kol, rebung, talas, dan masih banyak lagi serta cara memasak tradisional. Salah satu contoh makanan tradisional Cina adalah dimsum.<br>
                    <b>Jepang</b><br>
                    Karena letak geografis Jepang, maka masakan khas Jepang didominasi oleh  ikan dan seafood, yang menjadikannya salah satu yang paling sehat. Contoh makanan Jepang yang populer adalah sushi dan ramen. Makanan Jepang sangat tidak hanya memperhatikan dengan detail soal rasa, tetapi juga penampilan dari hidangan mereka.<br>
                    <b>India</b><br>
                    Masakan India merupakan makanan Asia dimana budaya, agama, dan tradisi memiliki pengaruh yang besar dalam terciptanya sebuah cita rasa yang ada. Ini didasarkan pada nasi, lentil, kacang-kacangan, serta semua jenis acar, yogurt, dan roti khas India. Untuk meningkatkan rasa makanan, orang India menggunakan sejumlah besar rempah-rempah (seperti cengkeh, jahe, kapulaga, kayu manis, jinten, kunyit), rempah-rempah (ketumbar) dan buah-buahan aromatik (asam, mangga, kismis) dan bijinya. <br>
                    <b>Vietnam</b><br>
                    Masakan Vietnam sekarang dianggap sebagai salah satu yang paling sehat di dunia. Yang sangat khas adalah banyaknya rempah segar, kaldu beraroma, nasi atau mie telur, serta kecap ikan, yang lebih populer daripada kecap negara-negara Asia lainnya.<br>
                    <b>Thailand</b><br>
                    Melimpahnya buah-buahan dan sayuran, pengaruh Cina dan India, dikombinasikan dengan kurangnya aturan tentang cara mengolah suatu hidangan membuat masakan Thailand menjadi salah satu masakan paling orisinal di dunia. Rempah-rempah segar (kemangi, ketumbar, lengkuas, cabai), santan, pasta kari, berbagai seafood dengan campuran rasa yang tidak biasa, membuat masakan Thailand begitu istimewa.<br>
                    <b>Korea</b><br>
                    Mungkin hal pertama yang terlintas mengenai makanan Korea adalah cita rasa yang pedas. Namun, hal ini tidak sepenuhnya benar, karena menurut tradisi makanan khas Korea memiliki rasa yang harmonis. Hidangan Korea menggunakan banyak bahan segar, alami dan berkualitas tinggi serta kandungan lemak yang rendah, lalu disajikan pada mangkuk - mangkuk kecil.
                    So, gimana? Jadi pingin kan merasakan uniknya cita rasa Asia? Don’t worry, kamu bisa merasakan authentic Asian food cuisine yang menggugah selera di <b>Black Canyon Surabaya.</b>
                    </p><br>
                    <br>
                   
                 </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 col-lg-offset-1 blog-gradient-right">
                    <div id="imaginary_container">
                        <div class="input-group stylish-input-group">
                            <input type="text" class="form-control" placeholder="Search Now">
                            <span class="input-group-addon"><button type="submit"><i class="icon dripicons-search"></i></button></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-12 col-lg-12 blog-border-responsive">
                            <div class="blog_recent_post-text">
                                <h1 class="blog_recent_post_head">RECENT POST</h1>
                                <hr class="bg6">
                                <ul class="blog_recent_post">
                                      <li>
                                        <a href="<?= base_url(); ?>blog_detail2" class="recent_post_link"><img src="<?= base_url(); ?>assets/template_white/images/produk/artikel2b.png" class="pull-left" alt="latest-news-image"></a>
                                        <h6 class="blog_widget_sub_head"><a href="<?= base_url(); ?>blog_detail2">Susah Dapat Inspirasi? Yuk, Datang ke Tempat Ini!</a></h6>
                                        <span><a href="#">ADMIN</a> | 12 JULY, 2021</span>
                                    </li>
                                    <li>
                                        <a href="<?= base_url(); ?>blog_detail" class="recent_post_link"><img src="<?= base_url(); ?>assets/template_white/images/produk/artikel1b.png" class="pull-left" alt="latest-news-image"></a>
                                        <h6 class="blog_widget_sub_head"><a href="<?= base_url(); ?>blog_detail">Ciri Cafe yang Asyik Buat <br>Dikunjungi</a></h6>
                                        <span><a href="#">ADMIN</a> | 12 JULY, 2021</span>
                                    </li>
                                     <li>
                                        <a href="<?= base_url(); ?>blog_detail3" class="recent_post_link"><img src="<?= base_url(); ?>assets/template_white/images/produk/artikel3b.png" class="pull-left" alt="latest-news-image"></a>
                                        <h6 class="blog_widget_sub_head"><a href="<?= base_url(); ?>blog_detail3">Kenali Berbagai Keunikan<br>
                                        Cita Rasa Asia</a></h6>
                                        <span><a href="#">ADMIN</a> | 11 JULY, 2021</span>
                                    </li>

                                    <li>
                                        <a href="<?= base_url(); ?>blog_detail4" class="recent_post_link"><img src="<?= base_url(); ?>assets/template_white/images/produk/artikel4b.png" class="pull-left" alt="latest-news-image"></a>
                                        <h6 class="blog_widget_sub_head"><a href="<?= base_url(); ?>blog_detail4">Quality family time di rumah aja? <br>Kenapa enggak? </a></h6>
                                        <span><a href="#">ADMIN</a> | 11 JULY, 2021</span>
                                    </li>
                                     <li>
                                        <a href="<?= base_url(); ?>blog_detail5" class="recent_post_link"><img src="<?= base_url(); ?>assets/template_white/images/produk/artikel5b.png" class="pull-left" alt="latest-news-image"></a>
                                        <h6 class="blog_widget_sub_head"><a href="<?= base_url(); ?>blog_detail5">Bukan Sekadar <br>Coffee House Biasa</a></h6>
                                        <span><a href="#">ADMIN</a> | 11 JULY, 2021</span>
                                    </li>
                                     <li>
                                        <a href="<?= base_url(); ?>blog_detail6" class="recent_post_link"><img src="<?= base_url(); ?>assets/template_white/images/produk/artikel6b.png" class="pull-left" alt="latest-news-image"></a>
                                        <h6 class="blog_widget_sub_head"><a href="<?= base_url(); ?>blog_detail6">Mau makan tapi takut salah?</a></h6>
                                        <span><a href="#">ADMIN</a> | 11 JULY, 2021</span>
                                    </li>

                                     <li>
                                        <a href="<?= base_url(); ?>blog_detail7" class="recent_post_link"><img src="<?= base_url(); ?>assets/template_white/images/produk/artikel7b.png" class="pull-left" alt="latest-news-image"></a>
                                        <h6 class="blog_widget_sub_head"><a href="<?= base_url(); ?>blog_detail7">Home Workspace <br>untuk Meningkatkan Produktivitas</a></h6>
                                        <span><a href="#">ADMIN</a> | 11 JULY, 2021</span>
                                    </li>
                                     <li>
                                        <a href="<?= base_url(); ?>blog_detail8" class="recent_post_link"><img src="<?= base_url(); ?>assets/template_white/images/produk/artikel8b.png" class="pull-left" alt="latest-news-image"></a>
                                        <h6 class="blog_widget_sub_head"><a href="<?= base_url(); ?>blog_detail8">Keuntungan Memulai<br> Bisnis di Usia Muda</a></h6>
                                        <span><a href="#">ADMIN</a> | 11 JULY, 2021</span>
                                    </li>
                                     <li>
                                        <a href="<?= base_url(); ?>blog_detail9" class="recent_post_link"><img src="<?= base_url(); ?>assets/template_white/images/produk/artikel9b.png" class="pull-left" alt="latest-news-image"></a>
                                        <h6 class="blog_widget_sub_head"><a href="<?= base_url(); ?>blog_detail9">Mau lebih Produktif? Yuk simak tips di bawah ini!</a></h6>
                                        <span><a href="#">ADMIN</a> | 11 JULY, 2021</span>
                                    </li>
                                   
                                   
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- blog_post ends -->

      <section id="footer" class="footer" style="background-color: white; padding-top: 0px; padding-bottom: 0px;">
        <div class="copyright-container">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                        <p class="copyrights boss-slider-info">&copy; 2021.All rights reserved.Powered by Black Canyon Coffee Indonesia <strong><a hef="#">blackcanyonindonesia.com</a></strong>.</p>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <div class="footer-social-icons">
                            <a href="https://www.facebook.com/blackcanyonindonesia-101323625562420" class="footer-icon-link facebook"><i class="fa fa-facebook " aria-hidden="true"></i></a>
                            <a href="https://twitter.com/IndonesiaBc" class="footer-icon-link twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                            <a href="https://www.instagram.com/blackcanyonindonesia/" class="footer-icon-link instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                            <a href="https://api.whatsapp.com/send?phone=6281383838497" class="footer-icon-link whatsapp"><i class="fa fa-whatsapp" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script src="<?= base_url(); ?>assets/template_white/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>assets/template_white/js/formValidation.min.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>assets/template_white/js/bootstrap.min.js"></script>
    <script src="<?= base_url(); ?>assets/template_white/js/wow.min.js"></script>
    <script src="<?= base_url(); ?>assets/template_white/js/imagesloaded.pkgd.js"></script>
    <script src="<?= base_url(); ?>assets/template_white/js/isotope.pkgd.min.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>assets/template_white/js/packery-mode.pkgd.min.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>assets/template_white/js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>assets/template_white/js/sliders.js"></script>

<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v11.0" nonce="WIP9U7yb"></script>
</body>

</html>
