<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'C_landing_page';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['index'] = 'C_landing_page/index';

$route['about_us'] = 'C_landing_page/about_us';

$route['menu'] = 'C_landing_page/menu';
$route['menubooks'] = 'C_landing_page/bukumenu';
$route['menufood'] = 'C_landing_page/menufoods';
$route['menudrinks'] = 'C_landing_page/menudrinks';

$route['blog'] = 'C_landing_page/blog';
$route['blog_detail1'] = 'C_landing_page/blog_detail';
$route['blog_detail2'] = 'C_landing_page/blog_detail2';
$route['blog_detail3'] = 'C_landing_page/blog_detail3';
$route['blog_detail4'] = 'C_landing_page/blog_detail4';
$route['blog_detail5'] = 'C_landing_page/blog_detail5';
$route['blog_detail6'] = 'C_landing_page/blog_detail6';
$route['blog_detail7'] = 'C_landing_page/blog_detail7';
$route['blog_detail8'] = 'C_landing_page/blog_detail8';
$route['blog_detail9'] = 'C_landing_page/blog_detail9';




$route['kontak'] = 'C_landing_page/kontak';

