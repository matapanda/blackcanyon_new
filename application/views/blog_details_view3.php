<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="155 characters of message matching text with a call to action goes here">
    <meta name="author" content="">
    <title>BLOG-DETAILS</title>
    <link rel="shortcut icon" href="<?= base_url(); ?>assets/template_white/images/favicon.png" type="image/x-icon">
    <link rel="icon" href="<?= base_url(); ?>assets/template_white/images/favicon.png" type="image/x-icon">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/animate.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/formValidation.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/webfont.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/owl.theme.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/fonts.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/style.css">
</head>

<body class="blog-details-page">
    <div class="grid_sys hidden" style="background-image:url('grid.png'); position:fixed; left:0;right:0;top:0;bottom:0; width:100%;z-index:9999999999999999999999999999999;min-height:1024px; background-position:center center;"></div>
    <div class="loader">
        <div class="loader-brand">
            
          <svg viewBox="0 0 700 300">
        <!-- Symbol-->
<svg version="1.0" xmlns="http://www.w3.org/2000/svg"
 width="500.000000pt" height="236.000000pt" viewBox="0 0 600.000000 236.000000"
 preserveAspectRatio="xMidYMid meet">

<g transform="translate(0.000000,236.000000) scale(0.100000,-0.100000)"
fill="#000000" stroke="none">
<path d="M1015 2347 c-43 -30 -94 -40 -143 -27 l-47 11 -37 -43 c-49 -56 -102
-159 -118 -227 -7 -29 -22 -117 -34 -194 -28 -184 -21 -178 -223 -186 l-148
-6 120 -14 c145 -17 351 -10 540 18 133 20 237 46 426 107 l106 34 -29 142
c-15 77 -34 153 -41 167 -16 33 -240 196 -291 211 -21 6 -43 13 -50 16 -6 2
-20 -2 -31 -9z m91 -112 c68 -28 175 -157 151 -181 -3 -4 -23 8 -44 25 -55 44
-121 70 -221 87 -112 19 -122 22 -122 40 0 10 14 14 53 14 28 1 63 7 77 15 32
18 62 19 106 0z"/>
<path d="M2641 2206 c-8 -9 -11 -108 -9 -342 l3 -329 282 -3 281 -2 31 30 c34
35 51 84 51 152 0 58 -11 87 -49 131 l-31 34 22 19 c54 49 66 176 23 251 -42
71 -50 73 -338 73 -203 0 -257 -3 -266 -14z m421 -133 c23 -21 24 -89 1 -115
-13 -15 -32 -18 -114 -18 -53 0 -104 3 -113 6 -20 8 -23 129 -3 137 6 3 57 6
111 6 79 1 103 -2 118 -16z m8 -292 c14 -27 13 -84 -3 -104 -11 -15 -28 -17
-127 -15 l-115 3 -3 54 c-5 83 -8 81 122 81 104 0 116 -2 126 -19z"/>
<path d="M3392 2208 c-9 -9 -12 -92 -12 -311 l0 -299 34 -34 34 -34 220 0
c191 0 221 2 226 16 11 28 6 130 -6 142 -8 8 -58 12 -158 12 -100 0 -150 4
-158 12 -9 9 -12 76 -12 241 0 138 -4 236 -10 248 -9 16 -22 19 -78 19 -40 0
-73 -5 -80 -12z"/>
<path d="M4216 2198 c-14 -22 -256 -647 -256 -661 0 -4 38 -7 84 -7 l84 0 28
70 27 70 118 0 c131 0 123 4 155 -80 l21 -55 86 -3 c48 -1 87 0 87 3 0 13
-252 660 -262 672 -7 8 -36 13 -84 13 -65 0 -75 -2 -88 -22z m125 -308 c15
-38 24 -72 21 -75 -3 -3 -32 -5 -65 -5 -43 0 -58 3 -54 13 2 6 14 40 27 75 12
34 28 62 34 62 6 0 23 -31 37 -70z"/>
<path d="M4805 2202 c-48 -29 -91 -86 -115 -149 -19 -52 -21 -73 -18 -198 3
-130 5 -144 31 -197 16 -32 47 -73 69 -93 l42 -35 232 0 c202 0 233 2 238 16
11 28 6 120 -6 132 -8 8 -61 12 -170 12 -183 0 -206 7 -239 72 -29 56 -25 192
7 235 40 54 73 63 245 63 134 0 158 2 163 16 11 28 6 120 -6 132 -9 9 -73 12
-227 12 -192 0 -219 -2 -246 -18z"/>
<path d="M5403 2213 c-10 -4 -13 -81 -13 -338 0 -383 -8 -355 97 -355 90 0 92
4 95 150 2 97 6 125 17 125 7 0 52 -56 100 -125 47 -68 97 -130 111 -137 36
-18 169 -17 176 1 3 8 -42 86 -102 175 l-107 161 107 161 c60 89 105 167 102
175 -7 18 -140 19 -176 1 -14 -7 -62 -66 -107 -132 -110 -159 -117 -159 -121
5 -2 84 -7 127 -15 132 -13 8 -144 10 -164 1z"/>
<path d="M1675 1824 c-105 -13 -185 -32 -300 -70 -196 -64 -426 -118 -590
-137 l-30 -3 27 -12 c15 -6 45 -28 67 -48 l41 -36 0 -99 c0 -54 -5 -110 -10
-125 -9 -23 -7 -31 12 -51 14 -15 28 -21 40 -17 23 7 25 -22 3 -46 -15 -16 -8
-18 96 -22 100 -4 114 -7 130 -26 14 -19 28 -22 83 -22 50 0 68 -4 72 -15 4
-8 18 -15 34 -15 15 0 47 -9 71 -20 47 -21 84 -26 74 -10 -3 5 -24 13 -46 16
-23 4 -46 15 -54 27 -29 42 9 50 58 13 18 -13 56 -36 85 -51 75 -37 136 -96
162 -157 12 -29 53 -98 91 -155 68 -102 69 -103 54 -132 -24 -48 -138 -131
-178 -131 -8 0 -25 21 -38 47 -18 36 -42 61 -95 98 -38 27 -98 74 -133 103
l-62 52 -75 0 c-68 0 -75 2 -84 23 -5 12 -9 17 -9 11 -1 -7 -35 -13 -89 -16
-93 -6 -152 -27 -152 -55 0 -12 7 -14 33 -9 69 14 175 26 217 25 25 0 65 2 90
5 50 8 76 -4 67 -28 -5 -13 -21 -16 -75 -16 -51 0 -73 -4 -83 -16 -31 -38 -72
-49 -181 -49 -172 0 -190 24 -208 270 l-11 160 25 22 25 21 -32 22 c-18 12
-43 37 -57 56 -29 38 -40 42 -40 15 0 -11 -9 -26 -21 -34 -19 -14 -20 -21 -13
-73 7 -56 6 -58 -11 -44 -10 8 -24 30 -32 48 -10 25 -18 32 -32 27 -11 -3 -44
-10 -75 -15 -31 -5 -110 -31 -176 -57 l-118 -48 -112 -159 -111 -159 34 -71
c91 -191 285 -375 522 -496 92 -47 252 -107 261 -98 2 3 -22 21 -55 41 -85 53
-156 121 -210 201 -53 78 -74 116 -67 116 3 0 61 -44 128 -98 205 -164 356
-262 427 -277 18 -4 58 -4 90 0 l56 8 -36 12 c-20 7 -51 21 -70 30 -36 18
-109 83 -109 97 0 20 61 4 139 -36 105 -54 198 -86 249 -86 48 0 172 32 260
67 98 39 264 155 353 246 77 77 189 229 189 255 0 20 -94 153 -136 193 -22 20
-72 62 -113 95 -40 32 -68 60 -62 62 18 6 155 -66 200 -105 23 -19 62 -65 88
-101 25 -37 50 -71 54 -76 4 -4 16 9 27 31 l20 39 -26 45 c-119 201 -241 277
-516 324 -115 19 -123 19 -129 -6 -7 -27 -31 -19 -35 12 -4 28 -29 59 -49 59
-9 0 -13 16 -13 60 0 57 1 60 28 66 128 27 302 104 375 164 41 34 77 96 77
131 0 70 -54 155 -119 188 -39 20 -107 31 -156 25z m-1178 -890 c3 -8 -2 -23
-11 -32 -15 -15 -17 -15 -32 0 -21 21 -11 48 16 48 11 0 23 -7 27 -16z m18
-86 c24 -43 32 -48 78 -48 39 0 59 -21 41 -45 -47 -64 -48 -66 -36 -108 9 -34
8 -45 -3 -56 -12 -12 -20 -12 -56 4 -41 19 -41 19 -81 -3 -24 -15 -46 -21 -59
-17 -18 6 -20 12 -14 55 6 45 5 49 -29 76 -47 39 -41 68 17 80 36 8 45 15 61
52 24 53 53 57 81 10z m183 -54 c3 -22 -21 -39 -39 -28 -11 6 -12 38 -2 48 13
13 38 1 41 -20z m-384 -19 c7 -19 -20 -46 -39 -39 -15 6 -20 35 -8 47 12 12
41 7 47 -8z m326 -200 c10 -12 10 -18 0 -30 -25 -30 -61 -7 -46 30 3 8 12 15
19 15 8 0 20 -7 27 -15z m1285 -79 c0 -53 -4 -69 -30 -106 -28 -40 -149 -147
-163 -143 -6 2 -54 125 -50 128 2 2 22 12 44 23 46 23 128 110 152 160 14 29
17 31 31 17 11 -11 16 -35 16 -79z m-1521 57 c10 -26 -23 -48 -42 -29 -8 8
-12 22 -9 30 8 21 43 20 51 -1z"/>
<path d="M170 1672 c0 -19 73 -71 160 -116 80 -41 318 -132 325 -124 2 2 1 40
-2 86 l-6 82 -91 0 c-136 0 -241 21 -368 75 -11 4 -18 3 -18 -3z"/>
<path d="M702 1528 c-13 -13 -16 -41 -6 -51 3 -3 14 -1 24 5 22 14 50 -4 50
-32 0 -32 69 -44 71 -12 3 40 -3 58 -27 80 -28 24 -92 30 -112 10z"/>
<path d="M2715 1309 c-68 -60 -80 -97 -80 -244 0 -148 13 -187 82 -244 l38
-31 180 2 180 3 0 60 0 60 -143 5 -144 5 -23 29 c-37 46 -46 130 -22 189 24
57 37 62 191 67 l141 5 0 60 0 60 -182 3 -182 2 -36 -31z"/>
<path d="M3371 1318 c-5 -13 -50 -130 -100 -260 -50 -131 -91 -244 -91 -253 0
-12 13 -15 61 -15 l60 0 27 60 26 61 94 -3 94 -3 22 -55 22 -55 59 -3 c32 -2
62 1 66 5 5 5 -38 128 -94 273 l-102 265 -68 3 c-62 3 -68 1 -76 -20z m103
-233 c24 -61 16 -77 -31 -73 -34 3 -38 6 -36 28 2 34 32 102 42 96 4 -3 16
-26 25 -51z"/>
<path d="M3782 1328 c-17 -17 -17 -509 0 -526 7 -7 35 -12 63 -12 28 0 56 5
63 12 8 8 12 56 12 150 0 76 3 138 8 138 4 0 47 -63 96 -141 58 -92 97 -144
113 -150 30 -11 133 -12 151 -1 9 7 12 68 10 273 l-3 264 -65 0 -65 0 -3 -147
c-1 -82 -6 -148 -11 -148 -4 0 -47 63 -95 140 -49 77 -97 145 -107 150 -26 14
-153 12 -167 -2z"/>
<path d="M4366 1332 c-2 -4 36 -74 85 -155 l89 -148 0 -108 c0 -70 4 -111 12
-119 7 -7 36 -12 65 -12 68 0 73 10 73 144 l0 105 85 141 c47 78 85 145 85
151 0 14 -114 11 -132 -3 -8 -7 -32 -42 -53 -78 -67 -113 -58 -110 -100 -38
-75 125 -78 128 -145 128 -32 0 -61 -3 -64 -8z"/>
<path d="M5036 1324 c-59 -21 -98 -56 -131 -115 -27 -49 -30 -62 -30 -144 1
-105 18 -149 84 -208 40 -36 119 -67 171 -67 88 0 190 63 233 144 33 63 31
203 -3 270 -14 27 -40 61 -59 77 -68 56 -181 74 -265 43z m140 -103 c87 -53
92 -224 8 -290 -53 -42 -115 -18 -154 59 -20 37 -17 129 5 174 18 38 66 76 95
76 8 0 29 -9 46 -19z"/>
<path d="M5477 1333 c-4 -3 -7 -125 -7 -269 l0 -264 23 -5 c13 -4 43 -5 68 -3
l44 3 3 148 c2 110 6 147 15 144 6 -2 49 -62 94 -133 45 -71 92 -137 104 -146
27 -23 150 -26 168 -4 8 9 11 90 9 272 l-3 259 -58 3 c-35 2 -64 -2 -73 -9
-11 -9 -14 -43 -14 -153 -1 -164 9 -165 -107 19 -41 66 -82 126 -91 133 -17
13 -163 17 -175 5z"/>
<path d="M1180 944 c0 -86 3 -113 12 -108 6 4 39 10 71 13 33 3 62 11 64 18 3
8 -13 10 -56 6 -56 -5 -61 -4 -61 14 0 15 13 23 55 37 55 17 90 44 44 34 -13
-3 -39 -9 -56 -13 -27 -6 -33 -4 -33 9 0 19 36 44 78 52 18 4 32 11 32 16 0
10 -10 8 -77 -9 -26 -6 -32 14 -11 35 9 9 4 12 -25 12 l-37 0 0 -116z"/>
<path d="M2630 580 l0 -30 1685 0 1685 0 0 30 0 30 -1685 0 -1685 0 0 -30z"/>
<path d="M2684 336 c-29 -29 -34 -41 -34 -83 0 -55 17 -86 60 -108 40 -21 89
-19 129 6 30 18 32 22 19 36 -14 13 -19 13 -47 -2 -61 -31 -121 4 -121 70 0
62 75 96 129 59 20 -15 25 -15 38 -2 24 25 -27 58 -89 58 -43 0 -54 -4 -84
-34z"/>
<path d="M2952 354 c-29 -20 -52 -66 -52 -104 0 -104 126 -156 201 -84 51 49
51 124 -1 175 -23 24 -37 29 -77 29 -28 0 -58 -7 -71 -16z m107 -34 c10 -5 24
-23 31 -39 15 -37 -2 -78 -41 -100 -48 -27 -109 18 -109 80 0 48 73 84 119 59z"/>
<path d="M3200 255 l0 -115 25 0 c23 0 25 4 25 40 l0 40 55 0 c48 0 55 3 55
19 0 17 -8 20 -52 23 -52 3 -53 4 -56 36 l-3 32 60 0 c54 0 61 2 61 20 0 18
-7 20 -85 20 l-85 0 0 -115z"/>
<path d="M3420 255 l0 -115 25 0 c23 0 25 4 25 40 l0 40 55 0 c48 0 55 3 55
19 0 17 -8 20 -52 23 -51 3 -53 4 -53 33 0 30 1 30 58 33 49 3 57 6 57 22 0
18 -7 20 -85 20 l-85 0 0 -115z"/>
<path d="M3640 255 l0 -115 90 0 c83 0 90 1 90 20 0 18 -7 20 -65 20 -63 0
-65 1 -65 25 0 24 3 25 60 25 53 0 60 2 60 20 0 18 -7 20 -60 20 l-60 0 0 30
0 30 65 0 c58 0 65 2 65 20 0 19 -7 20 -90 20 l-90 0 0 -115z"/>
<path d="M3880 255 l0 -115 95 0 c88 0 95 1 95 20 0 18 -7 20 -70 20 -68 0
-70 1 -70 25 0 24 3 25 60 25 53 0 60 2 60 20 0 18 -7 20 -61 20 -60 0 -60 0
-57 28 3 26 6 27 66 30 54 3 62 6 62 22 0 18 -7 20 -90 20 l-90 0 0 -115z"/>
<path d="M4266 354 c-18 -17 -21 -51 -7 -73 6 -10 3 -23 -11 -41 -45 -61 2
-122 78 -101 25 7 43 7 57 0 27 -15 43 2 26 28 -8 14 -9 25 -1 38 15 27 -16
51 -35 26 -11 -15 -13 -15 -29 1 -17 17 -17 19 4 41 27 29 29 73 3 87 -29 15
-67 12 -85 -6z m61 -30 c3 -8 -1 -22 -10 -31 -13 -14 -16 -13 -22 6 -11 35 19
59 32 25z m-11 -115 c16 -17 16 -22 4 -29 -37 -24 -69 -5 -50 30 14 25 22 25
46 -1z"/>
<path d="M4570 255 l0 -115 95 0 c88 0 95 1 95 20 0 18 -7 20 -70 20 -68 0
-70 1 -70 25 0 24 3 25 60 25 53 0 60 2 60 20 0 16 -8 19 -57 22 -54 3 -58 5
-58 28 0 24 3 25 63 28 54 3 62 6 62 22 0 18 -7 20 -90 20 l-90 0 0 -115z"/>
<path d="M4865 283 c-73 -170 -69 -153 -40 -153 17 0 29 8 37 25 10 22 17 25
65 25 47 0 55 -3 65 -25 7 -16 19 -25 35 -25 12 0 23 2 23 4 0 2 -22 55 -48
118 -42 98 -52 113 -74 116 -23 3 -29 -5 -63 -85z m80 -19 c19 -46 20 -44 -15
-44 -20 0 -30 5 -30 15 0 14 21 65 26 65 2 0 11 -16 19 -36z"/>
<path d="M5050 350 c0 -16 7 -20 35 -20 l35 0 0 -100 c0 -100 0 -100 25 -100
24 0 24 1 27 98 l3 97 38 3 c29 3 37 7 37 23 0 18 -8 19 -100 19 -93 0 -100
-1 -100 -20z"/>
<path d="M5297 363 c-4 -3 -7 -55 -7 -115 l0 -108 95 0 c88 0 95 1 95 20 0 18
-7 20 -70 20 -68 0 -70 1 -70 25 0 23 4 24 62 27 50 2 63 7 66 20 3 14 -7 17
-60 20 -60 3 -63 4 -63 28 0 24 3 25 63 28 54 3 62 6 62 22 0 18 -7 20 -83 20
-46 0 -87 -3 -90 -7z"/>
<path d="M5540 250 l0 -120 25 0 c22 0 25 4 25 35 0 32 3 35 28 35 23 0 33 -8
48 -35 14 -27 25 -35 46 -35 33 0 34 3 7 46 l-21 34 21 26 c21 27 26 58 15 89
-11 28 -60 45 -129 45 l-65 0 0 -120z m132 64 c14 -9 8 -52 -8 -58 -9 -3 -29
-6 -45 -6 -27 0 -29 2 -29 41 l0 41 37 -7 c21 -4 41 -9 45 -11z"/>
<path d="M5780 364 c0 -4 18 -39 40 -79 28 -52 40 -84 40 -114 0 -37 2 -41 25
-41 23 0 25 4 25 43 0 24 4 47 8 52 5 6 26 40 46 78 l37 67 -29 0 c-23 0 -32
-6 -41 -27 -6 -16 -20 -38 -29 -51 l-18 -23 -28 50 c-21 40 -33 51 -52 51 -13
0 -24 -3 -24 -6z"/>
</g>
</svg>
        </svg>
        </div>
    </div>
    <header class="header">
        <div class="top-container">
            <div class="navbar-primary affixnav" data-spy="affix" data-offset-top="100">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                         <div class="logo-image">
                            <a href="#" class="brand js-target-scroll">
                                <img src="<?= base_url(); ?>assets/template_white/images/logo_bc_white_4.png" alt="logo-image" class="logo-normal" />
                                <img src="<?= base_url(); ?>assets/template_white/images/logo_bc_black2.png" alt="logo-image" class="logo-hover" />
                            </a>
                        </div>
                    </div>
                    <div class="collapse navbar-collapse" id="navbar-collapse">
                        <ul class="nav navbar-nav navbar-right underline">
                            <li>
                                <a href="<?= base_url(); ?>" class="js-target-scroll">HOME</a>
                            </li>
                            <li>
                                <a href="<?= base_url(); ?>about_us" class="js-target-scroll">ABOUT US</a>
                            </li>
                            <li >
                                <a href="<?= base_url(); ?>menu" class="js-target-scroll">MENU</a>
                            </li>
                            <li class="active dropdown">
                                <a href="<?= base_url(); ?>blog" class="js-target-scroll">BLOG</a>
                            </li>
                            <li>
                                <a href="<?= base_url(); ?>kontak" class="js-target-scroll">CONTACT US</a>
                            </li>     
                        </ul>

                    </div>

                </div>
            </div>
        </div>
    </header>
    <!-- banner starts -->
    <section id="banner" class="banner">
        <div class="layer">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  text-center">
                        <div class="blog-text">
                            <h1 class="banner_heading">BLOG DETAIL</h1>
                         
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- banner ends -->
    <!-- blog-details -->
    <section id="blog_details" class="blog_post-container blog_details">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 blog-gradient-left">
                    <img class="img-responsive" src="<?= base_url(); ?>assets/template_white/images/produk/artikel3.png" alt="blog">
                    <h1 class="blog_details_head">Kenali Berbagai Keunikan <br> Cita Rasa Asia </h1>
                   
                    
                    <p style=" text-align: justify;">&ensp; &ensp; Setiap negara memiliki keunikannya sendiri. Tidak hanya bahasa, budaya, dan mode, namun juga selera masakan yang berbeda. Cara memasak, bahan makanan, dan kebiasaan tersebut menciptakan berbagai keunikan cita rasa pada setiap negara. Tidak terkecuali negara-negara di benua Asia. Tidak semua makanan Asia memiliki cita rasa yang sama. Walaupun makanan Asia terkenal dengan kulinernya yang khas menggunakan bumbu rempah yang bervariatif, namun masing-masing negara memiliki keunikannya sendiri. Nah, yuk simak berbagai keunikan dari cita rasa Asia!</p>
                    <p style="text-align: justify;"><b>Cina</b><br>
                    Wilayah yang luas, perbedaan budaya dan iklim negara, membuat masakan Cina paling beragam di antara yang lain. Namun, faktanya pada keberagaman tersebut, orang Cina lebih memilih untuk tetap berpegang pada resep yang sudah teruji dan turun menurun. Masakan Cina memiliki ciri khas menggunakan berbagai sayuran seperti tauge, kol, rebung, talas, dan masih banyak lagi serta cara memasak tradisional. Salah satu contoh makanan tradisional Cina adalah dimsum.<br>
                    <b>Jepang</b><br>
                    Karena letak geografis Jepang, maka masakan khas Jepang didominasi oleh  ikan dan seafood, yang menjadikannya salah satu yang paling sehat. Contoh makanan Jepang yang populer adalah sushi dan ramen. Makanan Jepang sangat tidak hanya memperhatikan dengan detail soal rasa, tetapi juga penampilan dari hidangan mereka.<br>
                    <b>India</b><br>
                    Masakan India merupakan makanan Asia dimana budaya, agama, dan tradisi memiliki pengaruh yang besar dalam terciptanya sebuah cita rasa yang ada. Ini didasarkan pada nasi, lentil, kacang-kacangan, serta semua jenis acar, yogurt, dan roti khas India. Untuk meningkatkan rasa makanan, orang India menggunakan sejumlah besar rempah-rempah (seperti cengkeh, jahe, kapulaga, kayu manis, jinten, kunyit), rempah-rempah (ketumbar) dan buah-buahan aromatik (asam, mangga, kismis) dan bijinya. <br>
                    <b>Vietnam</b><br>
                    Masakan Vietnam sekarang dianggap sebagai salah satu yang paling sehat di dunia. Yang sangat khas adalah banyaknya rempah segar, kaldu beraroma, nasi atau mie telur, serta kecap ikan, yang lebih populer daripada kecap negara-negara Asia lainnya.<br>
                    <b>Thailand</b><br>
                    Melimpahnya buah-buahan dan sayuran, pengaruh Cina dan India, dikombinasikan dengan kurangnya aturan tentang cara mengolah suatu hidangan membuat masakan Thailand menjadi salah satu masakan paling orisinal di dunia. Rempah-rempah segar (kemangi, ketumbar, lengkuas, cabai), santan, pasta kari, berbagai seafood dengan campuran rasa yang tidak biasa, membuat masakan Thailand begitu istimewa.<br>
                    <b>Korea</b><br>
                    Mungkin hal pertama yang terlintas mengenai makanan Korea adalah cita rasa yang pedas. Namun, hal ini tidak sepenuhnya benar, karena menurut tradisi makanan khas Korea memiliki rasa yang harmonis. Hidangan Korea menggunakan banyak bahan segar, alami dan berkualitas tinggi serta kandungan lemak yang rendah, lalu disajikan pada mangkuk - mangkuk kecil.
                    So, gimana? Jadi pingin kan merasakan uniknya cita rasa Asia? Don’t worry, kamu bisa merasakan authentic Asian food cuisine yang menggugah selera di <b>Black Canyon Surabaya.</b>
                    </p><br>
                    <br>
                   
                 </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 col-lg-offset-1 blog-gradient-right">
                    <div id="imaginary_container">
                        <div class="input-group stylish-input-group">
                            <input type="text" class="form-control" placeholder="Search Now">
                            <span class="input-group-addon"><button type="submit"><i class="icon dripicons-search"></i></button></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-12 col-lg-12 blog-border-responsive">
                            <div class="blog_recent_post-text">
                                <h1 class="blog_recent_post_head">RECENT POST</h1>
                                <hr class="bg6">
                                <ul class="blog_recent_post">
                                      <li>
                                        <a href="<?= base_url(); ?>blog_detail2" class="recent_post_link"><img src="<?= base_url(); ?>assets/template_white/images/produk/artikel2b.png" class="pull-left" alt="latest-news-image"></a>
                                        <h6 class="blog_widget_sub_head"><a href="<?= base_url(); ?>blog_detail2">Susah Dapat Inspirasi? Yuk, Datang ke Tempat Ini!</a></h6>
                                        <span><a href="#">ADMIN</a> | 12 JULY, 2021</span>
                                    </li>
                                    <li>
                                        <a href="<?= base_url(); ?>blog_detail" class="recent_post_link"><img src="<?= base_url(); ?>assets/template_white/images/produk/artikel1b.png" class="pull-left" alt="latest-news-image"></a>
                                        <h6 class="blog_widget_sub_head"><a href="<?= base_url(); ?>blog_detail">Ciri Cafe yang Asyik Buat <br>Dikunjungi</a></h6>
                                        <span><a href="#">ADMIN</a> | 12 JULY, 2021</span>
                                    </li>
                                     <li>
                                        <a href="<?= base_url(); ?>blog_detail3" class="recent_post_link"><img src="<?= base_url(); ?>assets/template_white/images/produk/artikel3b.png" class="pull-left" alt="latest-news-image"></a>
                                        <h6 class="blog_widget_sub_head"><a href="<?= base_url(); ?>blog_detail3">Kenali Berbagai Keunikan<br>
                                        Cita Rasa Asia</a></h6>
                                        <span><a href="#">ADMIN</a> | 11 JULY, 2021</span>
                                    </li>

                                    <li>
                                        <a href="<?= base_url(); ?>blog_detail4" class="recent_post_link"><img src="<?= base_url(); ?>assets/template_white/images/produk/artikel4b.png" class="pull-left" alt="latest-news-image"></a>
                                        <h6 class="blog_widget_sub_head"><a href="<?= base_url(); ?>blog_detail4">Quality family time di rumah aja? <br>Kenapa enggak? </a></h6>
                                        <span><a href="#">ADMIN</a> | 11 JULY, 2021</span>
                                    </li>
                                     <li>
                                        <a href="<?= base_url(); ?>blog_detail5" class="recent_post_link"><img src="<?= base_url(); ?>assets/template_white/images/produk/artikel5b.png" class="pull-left" alt="latest-news-image"></a>
                                        <h6 class="blog_widget_sub_head"><a href="<?= base_url(); ?>blog_detail5">Bukan Sekadar <br>Coffee House Biasa</a></h6>
                                        <span><a href="#">ADMIN</a> | 11 JULY, 2021</span>
                                    </li>
                                     <li>
                                        <a href="<?= base_url(); ?>blog_detail6" class="recent_post_link"><img src="<?= base_url(); ?>assets/template_white/images/produk/artikel6b.png" class="pull-left" alt="latest-news-image"></a>
                                        <h6 class="blog_widget_sub_head"><a href="<?= base_url(); ?>blog_detail6">Mau makan tapi takut salah?</a></h6>
                                        <span><a href="#">ADMIN</a> | 11 JULY, 2021</span>
                                    </li>

                                     <li>
                                        <a href="<?= base_url(); ?>blog_detail7" class="recent_post_link"><img src="<?= base_url(); ?>assets/template_white/images/produk/artikel7b.png" class="pull-left" alt="latest-news-image"></a>
                                        <h6 class="blog_widget_sub_head"><a href="<?= base_url(); ?>blog_detail7">Home Workspace <br>untuk Meningkatkan Produktivitas</a></h6>
                                        <span><a href="#">ADMIN</a> | 11 JULY, 2021</span>
                                    </li>
                                     <li>
                                        <a href="<?= base_url(); ?>blog_detail8" class="recent_post_link"><img src="<?= base_url(); ?>assets/template_white/images/produk/artikel8b.png" class="pull-left" alt="latest-news-image"></a>
                                        <h6 class="blog_widget_sub_head"><a href="<?= base_url(); ?>blog_detail8">Keuntungan Memulai<br> Bisnis di Usia Muda</a></h6>
                                        <span><a href="#">ADMIN</a> | 11 JULY, 2021</span>
                                    </li>
                                     <li>
                                        <a href="<?= base_url(); ?>blog_detail9" class="recent_post_link"><img src="<?= base_url(); ?>assets/template_white/images/produk/artikel9b.png" class="pull-left" alt="latest-news-image"></a>
                                        <h6 class="blog_widget_sub_head"><a href="<?= base_url(); ?>blog_detail9">Mau lebih Produktif? Yuk simak tips di bawah ini!</a></h6>
                                        <span><a href="#">ADMIN</a> | 11 JULY, 2021</span>
                                    </li>
                                   
                                   
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- blog_post ends -->

      <section id="footer" class="footer" style="background-color: white; padding-top: 0px; padding-bottom: 0px;">
        <div class="copyright-container">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                       <p class="copyrights boss-slider-info">&copy; 2021.All rights reserved.Powered by Black Canyon Coffee Indonesia <strong><a hef="#">blackcanyonindonesia.co.id</a></strong>.</p>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <div class="footer-social-icons">
                            <a href="https://www.facebook.com/blackcanyonindonesia-101323625562420" class="footer-icon-link facebook"><i class="fa fa-facebook " aria-hidden="true"></i></a>
                            <a href="https://twitter.com/IndonesiaBc" class="footer-icon-link twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                            <a href="https://www.instagram.com/blackcanyonindonesia/" class="footer-icon-link instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                            <a href="https://api.whatsapp.com/send?phone=6281383838497" class="footer-icon-link whatsapp"><i class="fa fa-whatsapp" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script src="<?= base_url(); ?>assets/template_white/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>assets/template_white/js/formValidation.min.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>assets/template_white/js/bootstrap.min.js"></script>
    <script src="<?= base_url(); ?>assets/template_white/js/wow.min.js"></script>
    <script src="<?= base_url(); ?>assets/template_white/js/imagesloaded.pkgd.js"></script>
    <script src="<?= base_url(); ?>assets/template_white/js/isotope.pkgd.min.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>assets/template_white/js/packery-mode.pkgd.min.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>assets/template_white/js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>assets/template_white/js/sliders.js"></script>

<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v11.0" nonce="WIP9U7yb"></script>
</body>

</html>
