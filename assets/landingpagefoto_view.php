<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Website Black Canyon Indonesia">
    <meta name="author" content="">
    <title>BLACKCANYON</title>
    <link rel="shortcut icon" href="<?= base_url(); ?>assets/template_white/images/favicon.png" type="image/x-icon">
    <link rel="icon" href="<?= base_url(); ?>assets/template_white/images/favicon.png" type="image/x-icon">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/animate.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/formValidation.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/webfont.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/owl.theme.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/fonts.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/style.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/template_white/css/icofont.min.css">
</head>

<body class="dummy-cls">
    <div class="grid_sys hidden" style="background-image:url('grid.png'); position:fixed; left:0;right:0;top:0;bottom:0; width:100%;z-index:9999999999999999999999999999999;min-height:1024px; background-position:center center;"></div>
   <div class="grid_sys hidden" style="background-image:url('grid.png'); position:fixed; left:0;right:0;top:0;bottom:0; width:100%;z-index:9999999999999999999999999999999;min-height:1024px; background-position:center center;"></div>
    <div class="loader">
        <div class="loader-brand">
            
            <svg viewBox="0 0 1300 300">
        <!-- Symbol-->
        <symbol id="s-text"> 
            <text text-anchor="middle" x="50%" y="50%" dy=".35em">
                <tspan   class="bold">B</tspan >LACK<tspan  class="bold">CANYON</tspan >
            </text> 
        </symbol>
        <!-- Duplicate symbols-->
        <use class="text" xlink:href="#s-text"></use>
        <use class="text" xlink:href="#s-text"></use>
        <use class="text" xlink:href="#s-text"></use>
        <use class="text" xlink:href="#s-text"></use>
        <use class="text" xlink:href="#s-text"></use>
        </svg>

        </div>
    </div>
    <header class="header">
        <div class="top-container">
            <div class="navbar-primary affixnav" data-spy="affix" data-offset-top="100">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                        <div class="logo-image">
                            <a href="<?= base_url(); ?>assets/template_white/#" class="brand js-target-scroll">
                                <img src="<?= base_url(); ?>assets/template_white/images/logo-white.png" alt="logo-image" class="logo-normal" />
                                <img src="<?= base_url(); ?>assets/template_white/images/logo-blacks.png" alt="logo-image" class="logo-hover" />
                            </a>
                        </div>
                    </div>
                     <div class="collapse navbar-collapse" id="navbar-collapse">
                        <ul class="nav navbar-nav navbar-right underline">
                            <li class="active dropdown">
                                <a href="<?= base_url(); ?>" class="js-target-scroll">HOME</a>
                            </li>
                            <li>
                                <a href="<?= base_url(); ?>c_landing_page/about_us" class="js-target-scroll">ABOUT US</a>
                            </li>
                            <li >
                                <a href="<?= base_url(); ?>c_landing_page/menu" class="js-target-scroll">MENU</a>
                            </li>
                            <li >
                                <a href="<?= base_url(); ?>c_landing_page/blog" class="js-target-scroll">BLOG</a>
                            </li>
                            <li>
                                <a href="<?= base_url(); ?>c_landing_page/kontak" class="js-target-scroll">CONTACT US</a>
                            </li>     
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="content">
        <!-- banner starts -->
        <section id="slider" class="slider">
            <div class="item">
                <div class="image-caption hidden-xs">
                    <div class="container">
                        <div class="row" style="text-align: center;">
                            <div class="col-sm-12 col-md-12 col-lg-12 ">
                           
                            <!--     <h1 class="slider-heading fadeInRight wow" data-wow-duration="1s">A taste from paradise, available on earth</h1> -->
                           
                               <!--  <p class="boss-slider-info fadeInDown wow" style="font-size: 3em;" data-wow-duration="1s">A taste from paradise, available,br on earth</p> --><br><br><br><br><br><br>
                                <a href="<?= base_url(); ?>c_landing_page/menu" class="btn btn-primary">Our Menu</a>
                            </div>
                        </div>
                    </div>
                </div>
                <img class="img-responsive" src="<?= base_url(); ?>assets/template_white/images/produk/slide1a.png" alt="banner-1">
            </div>
            <div class="item">
                <div class="image-caption hidden-xs">
                    <div class="row" style="text-align: center;">
                        <div class="row">
                           <div class="col-sm-12 col-md-12 col-lg-12 ">
                     
                              <!--   <h1 class="slider-heading fadeInRight wow" data-wow-duration="1s">BLACK CANYON COFFEE <BR>INDONESIA</h1> -->
                             
                             <!--    <p class="boss-slider-info fadeInDown wow" style="font-size: 3em;" data-wow-duration="1s">A beautiful place with a touch of Thailand’s unique culture</p>  <br> <hr><br> --><br><br><br><br><br><br>
                                <a href="<?= base_url(); ?>c_landing_page/menu" class="btn btn-primary">Our Menu</a>
                            </div>
                        </div>
                    </div>
                </div>
                <img class="img-responsive" src="<?= base_url(); ?>assets/template_white/images/produk/slide2a.png" alt="banner-2">
            </div>
            <div class="item">
                <div class="image-caption hidden-xs">
                    <div class="container">
                       <div class="row" style="text-align: center;">
                             <div class="col-sm-12 col-md-12 col-lg-12 ">
                               <!--  <h1 class="slider-heading fadeInRight wow" data-wow-duration="1s">BLACK CANYON COFFEE <BR>INDONESIA</h1> -->
                          <!-- 
                                <p class="boss-slider-info fadeInDown wow" style="font-size: 3em;" data-wow-duration="1s">A soul-satisfying delight of Asian cuisine and beverage</p> --><br><br><br><br><br><br>
                                <a href="<?= base_url(); ?>c_landing_page/menu" class="btn btn-primary">Our Menu</a>
                            </div>
                        </div>
                    </div>
                </div>
                <img class="img-responsive" src="<?= base_url(); ?>assets/template_white/images/produk/slide3a.png" alt="banner-3">
            </div>
        </section>
        <!-- banner ends -->
        <main class="main">
            <!-- our-history starts -->
            <section id="our-history startchange" class="our-history">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="history-image" style="width: 70%;">
                                <img class="img-responsive" src="<?= base_url(); ?>assets/template_white/images/aboutus.gif" alt="history-image" />
                              <!--  <video width="100%" loop="true" controls autoplay="1">
                                  <source src="<?= base_url(); ?>assets/template_white/video/12juli.mp4" type="video/mp4">
                                  <source src="<?= base_url(); ?>assets/template_white/video/12juli.ogg" type="video/ogg">
                                  Your browser does not support the video tag.
                                </video> -->
                                  
                                 </div>
                             
                           
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="history-text fadeInRight wow" data-wow-duration="1s">
                                <h1>ABOUT US</h1>
                                <hr class="hr-bg">
                                <p><span>  Fresh air, coffee smell, good cuisine.<br>
                                 Where else can you get a soothing ambiance, delicious food, and excellent service all in one place?</span></p>
                                <a href="<?= base_url(); ?>c_landing_page/about_us" class="btn btn-primary">Find out more about us  <i class="icofont icofont-arrow-right" style="font-size: 20px;"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
           
            <!-- services starts -->
            <section id="services" class="services" style="padding-top: 0px;"> 
                <div class="container">
                    <div class="row homev1-services">
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 responsive-col1-change">
                            <div class="services-text text-right">
                                <hr class="hr-bg bg2 hidden-xs">
                                <h1 class="services-heading">A little <BR>piece of heaven</h1>
                                <p>Our delish food and beverages are produced with the finest ingredients, by the best chefs and baristas, and served with pleasure. 
                                    Just sit, be relax, and spoil your tongue with our special menu.</p>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 text-right service-mediaquery">
                            <img class="img-responsive fadeInRight wow" data-wow-duration="1s" src="<?= base_url(); ?>assets/template_white/images/produk/about_us4.jpeg" alt="ewbdesign-image" />
                        </div>
                    </div>
                    <div class="row homev1-services">
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 service-mediaquery">
                            <img class="img-responsive fadeInLeft wow" data-wow-duration="1s" src="<?= base_url(); ?>assets/template_white/images/produk/about_us2.jpeg" alt="ewbdesign-image" />
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 responsive-col1-change">
                            <div class="services-text  text-left">
                                <hr class="hr-bg bg2 stock-bg2 hidden-xs">
                                <h1 class="services-heading">A perfect cup <br>of coffee</h1>
                                <p>Our goal is to serve specialty coffee. 
                                Enjoy a pleasant eating experience in a wonderful atmosphere of Thailand vibes and a taste of paradise, while sipping a cup of delicious freshly brewed coffee</p>
                            </div>
                        </div>
                    </div>
                   
                </div>
            </section>
           
           
            <!-- blog-post starts ini mas-->
            <section id="blog-post" class="blog-post" style="padding-top: 2px;">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-offset-1 col-sm-10 col-md-offset-2 col-md-8 col-lg-offset-3 col-lg-6 text-center">
                            <div class="blog-text fadeInDown wow">
                        
                                <h1>RECENT BLOG</h1>
                                <hr class="hr-bg bg3">
                                <p></p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="owl-blog-carousel owl-theme">
                             <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="item">
                                    <img class="img-responsive" src="<?= base_url(); ?>assets/template_white/images/produk/artikel.png" alt="blog" />
                                    <div class="blog-content">
                                         <a href="<?= base_url(); ?>c_landing_page/blog_detail">
                                            <h3 class="blog-title">Ciri Cafe yang Asyik Buat Dikunjungi</h3>
                                        </a>
                                        <span class="blog-subhead">Saat ini ada banyak sekali pilihan tempat nongkrong...</span>
                                       
                                         <div class="blog-subhead blog-button center">
                                          <a href="<?= base_url(); ?>c_landing_page/blog_detail" class="btn btn-secondary">READ MORE</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                 <div class="item">
                                    <img class="img-responsive" src="<?= base_url(); ?>assets/template_white/images/produk/artikel2.png" alt="blog" />
                                    <div class="blog-content">
                                         <a href="<?= base_url(); ?>c_landing_page/blog_detail2">
                                            <h3 class="blog-title">Susah Dapat Inspirasi? Yuk, Datang ke Tempat Ini!</h3>
                                        </a>
                                        <span class="blog-subhead">Sulitnya mendapatkan inspirasi itu bukan hanya dirasakan ...</span>
                                       
                                         <div class="blog-subhead blog-button center">
                                          <a href="<?= base_url(); ?>c_landing_page/blog_detail2" class="btn btn-secondary">READ MORE</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                             <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                 <div class="item">
                                    <img class="img-responsive" src="<?= base_url(); ?>assets/template_white/images/produk/artikel3.png" alt="blog" />
                                    <div class="blog-content">
                                         <a href="<?= base_url(); ?>c_landing_page/blog_detail3">
                                            <h3 class="blog-title">Kenali Berbagai Keunikan Cita Rasa Asia </h3>
                                        </a>
                                        <span class="blog-subhead">Setiap negara memiliki keunikannya sendiri...</span>
                                       
                                         <div class="blog-subhead blog-button center">
                                          <a href="<?= base_url(); ?>c_landing_page/blog_detail3" class="btn btn-secondary">READ MORE</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="item">
                                    <img class="img-responsive" src="<?= base_url(); ?>assets/template_white/images/produk/artikel.png" alt="blog" />
                                    <div class="blog-content">
                                         <a href="<?= base_url(); ?>c_landing_page/blog_detail">
                                            <h3 class="blog-title">Ciri Cafe yang Asyik Buat Dikunjungi</h3>
                                        </a>
                                        <span class="blog-subhead">Saat ini ada banyak sekali pilihan tempat nongkrong...</span>
                                       
                                         <div class="blog-subhead blog-button center">
                                          <a href="<?= base_url(); ?>c_landing_page/blog_detail" class="btn btn-secondary">READ MORE</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                 <div class="item">
                                    <img class="img-responsive" src="<?= base_url(); ?>assets/template_white/images/produk/artikel2.png" alt="blog" />
                                    <div class="blog-content">
                                         <a href="<?= base_url(); ?>c_landing_page/blog_detail2">
                                            <h3 class="blog-title">Susah Dapat Inspirasi? Yuk, Datang ke Tempat Ini!</h3>
                                        </a>
                                        <span class="blog-subhead">Sulitnya mendapatkan inspirasi itu bukan hanya dirasakan ...</span>
                                       
                                         <div class="blog-subhead blog-button center">
                                          <a href="<?= base_url(); ?>c_landing_page/blog_detail2" class="btn btn-secondary">READ MORE</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                 <div class="item">
                                    <img class="img-responsive" src="<?= base_url(); ?>assets/template_white/images/produk/artikel3.png" alt="blog" />
                                    <div class="blog-content">
                                         <a href="<?= base_url(); ?>c_landing_page/blog_detail3">
                                            <h3 class="blog-title">Kenali Berbagai Keunikan Cita Rasa Asia </h3>
                                        </a>
                                        <span class="blog-subhead">Setiap negara memiliki keunikannya sendiri...</span>
                                       
                                         <div class="blog-subhead blog-button center">
                                          <a href="<?= base_url(); ?>c_landing_page/blog_detail3" class="btn btn-secondary">READ MORE</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                            <div class="blog-button">
                                <a href="<?= base_url(); ?>c_landing_page/blog" class="btn btn-secondary">ALL POST</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
           
   <section id="footer" class="footer" style="background-color: white; padding-top: 0px; padding-bottom: 0px;">
        <div class="copyright-container">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                        <p class="copyrights boss-slider-info">&copy; 2021.All rights reserved.Powered by Black Canyon Coffee Indonesia <strong><a hef="#">blackcanyonindonesia.com</a></strong>.</p>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <div class="footer-social-icons">
                            <a href="https://www.facebook.com/blackcanyonindonesia-101323625562420" class="footer-icon-link facebook"><i class="fa fa-facebook " aria-hidden="true"></i></a>
                            <a href="https://twitter.com/IndonesiaBc" class="footer-icon-link twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                            <a href="https://www.instagram.com/blackcanyonindonesia/" class="footer-icon-link instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                            <a href="https://api.whatsapp.com/send?phone=6281383838497" class="footer-icon-link whatsapp"><i class="fa fa-whatsapp" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script src="<?= base_url(); ?>assets/template_white/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>assets/template_white/js/formValidation.min.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>assets/template_white/js/bootstrap.min.js"></script>
    <script src="<?= base_url(); ?>assets/template_white/js/wow.min.js"></script>
    <script src="<?= base_url(); ?>assets/template_white/js/imagesloaded.pkgd.js"></script>
    <script src="<?= base_url(); ?>assets/template_white/js/isotope.pkgd.min.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>assets/template_white/js/packery-mode.pkgd.min.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>assets/template_white/js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>assets/template_white/js/sliders.js"></script>


</body>

</html>
